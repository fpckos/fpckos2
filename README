
* https://bitbucket.org/fpckos/
* ftp://ftp.kolibrios.org/users/bw/

Не сделано, сделано на отъебись и другие особенности:

* ничего не тестировалось и если что-то есть, то это не значит что оно работает, классика;
* нельзя создавать динамические библиотеки;
* нельзя подключать динамические библиотеки;
* при сборке KEX-а не инициализированные секции заливаются нулями, как и "дыры" между секциями;
* есть обработка параметров командной строки;
* поддерживается директива `apptype`, по-умолчанию, значение `console`, см. так же раздел "Терминал" ниже;
* стандартный вывод (и ошибки) происходит на отладочную доску (sysfn 63.1), см. так же раздел "Терминал" ниже;
* стандартный ввод осуществялется с отладочной доски (sysfn 63.2);
* перенаправление потоков ввода/ввыода отсутствует;
* есть переменные окружения, см. ниже;
* есть многопоточность (см. первый пункт);
  - размер стека 256KB;
  - есть в KOS (sysfn 69.4/5), но не реализовано в RTL suspend/resume потока;
  - есть threadvar/tls (см. первый пункт);
  - `EndThread` отсутствует, есть только `KillThread`, который будет приводить к утечкам памяти (вероятно) и
    блокировкам конкурирующих потоков (возможно);
  - `EnterCriticalSection` есть, но сделан на тупом цикле (не нашёл в системе mutex-ов);
* есть исключения (см. первый пункт);
* стек фиксировнного размера (скорее всего);
* аппаратные прерывания не обрабатываются, например FPU (sysfn 68.1);
* и т.д.

Выпилено овер дохрена пакетов, они просто не портировались и не проверялись.
Примеры и альтернативные платформы сохранены, но сборка (компилятора и своих демок)
проверялась только под Linux (x86_64).

В основном работа велась над RTL и немного над компилятором.

Если чего-то не хватает, берём из исходников FreePascal и пилим.

Используется нативный FPC-компилятор (например ppcx64 для Linux x86_64) для сборки
нативного-кросскомпилятора для текущей ОС, но уже с поддержкой платформы KolibriOS x86
(например ppc386 для Linux x86_64). Так же нативный компилятор используется для сборки
fpcmake, который знает про i386-kolibri. Повсюду используется GNU make.

Файл с конфигурацией "fpc.cfg" будет искаться по следующим путям (или не будет):

* ./fpc.cfg
* $HOME/fpc.cfg
* $PPC_CONFIG_PATH/fpc.cfg
* /sys/settings/fpc.cfg

Сделать до 2050:

* fpckos-devel
* ресурсы, см. `compiler/intres.inc`, `compiler/comprsrc.pas`, `system_i386_kolibri_info.res`, `windres`, `fpcres` и т.д.
* x86_64-w64-mingw32-windres -i simple.rc -o simple.res
* PIC, см. `FPC_PIC`, `tf_no_pic_supported`, `tf_pic_uses_got`, `cs_create_pic` и т.д.
* отладка, см. `.stab` и `.stabstr` в `lineinfo` и `exeinfo` при `make debug`


Терминал
--------

Реализован примитивный и кривой терминал, который доступен при сборке консольных приложений
(по-умолчанию): `{$apptype console}`. Что-бы этой дряни не было, нужно собирать в режиме GUI:
`{$apptype gui}`.

Терминал создаётся первым консольным fpc-приложением, все остальные (консольные fpc-приложения), запускаемые
через `ExecuteProcess` или `TProcess`, используют его, а не создают свой экземпляр.

Особенности:

* запускается в отдельном потоке и для завершения требует явного закрытия, иначе процесс приложения
  продолжит работать (ждать завершения потока терминала);
* ввод отсутствует;
* при завершении процесса (главного потока) в заголовок окна выводится код завершения, информация о других процессах,
  использующих этот терминал не доступна;
* закрытие окна выполняется кнопкой, Alt-F4 или Ctrl-Q, при этом все потоки и процессы
  завязанные на этот терминал продолжат работать (вероятно, подвисая на выводе, не проверялось),
  никаких сигналов никому не посылается;
* связь с терминалом (как можно догадаться) строго одностороняя.

См. реализацию в RTL в "kolibri/sysinitconsole.pp" и "kolibri/kos_term.inc".
Кроме RTL, изменения вносились и в сам компилятор.

См. примеры "misc/xpc1.pp" и "misc/xpc2.pp" (xpc1.kex запускает xpc2.kex и оба выводят в один терминал).


Переменные окружения
--------------------

Переменные либо передаются через `TProcess` при запуске, либо читаются из файлов "/sys/settings/.environ" и
из ".environ", находящегося в корне диска с приложением, при этом значения в из последнего файла имеют больший приоритет.
Поддерживается элементарная sh-подобная подстановка (с волшебным словом __HERE__), пример файла "/hd0/1/.environ":

```shell
FPCROOT=$__HERE__/fpckos-2.4.4
FPCDIR=$FPCROOT/lib/fpc/2.4.4
PPC_CONFIG_PATH=$FPCROOT
PATH=/sys:$FPCROOT/bin:$FPCDIR
```

Из бонусов, теперь можно пользоваться "PATH", эта переменная обрабатывается в `ExecuteProcess` и в `TProcess`
(т.е. по задумке, особо не проверял).

См. реализацию в RTL в "kolibri/sysinitkos.inc" и "kolibri/kos_xpc.inc", и в пакете "fpc-process".

См. примеры "misc/xpc1.pp" и "misc/xpc2.pp" (xpc1.kex запускает xpc2.kex с набором переменных окружения).


Windows ресурсы
---------------

Для компиляции RC в RES всё так же нужен `windres`, в Linux его можно найти в пакетах `mingw32`, для Windows
утилита входит в состав FPC. Компилятору можно явно указать на эту утилиту ключём "-FC". В примерах RC предварительно
был конвертирован в RES руками, а в самих исходниках в директиве `{$r ...}` указывается не RC, а RES.

API реализован (спустя рукава). Поддержки языков нет.

См. реализацию в RTL в "inc/fpintres.pp", "int/sysres.inc" и "kolibri/kos_winres.inc".
Кроме RTL, изменения вносились и в сам компилятор.

См. примеры "misc/resdevel.pp" и "misc/resapp.pp".


Сборка нативного кросс-компилятора и RTL
----------------------------------------

Всё что дальше будет происходить, выполняется под Linux x86_64 и с использованием bootstrap-сборки FPC.
Качаем минимальную сборку FPC. Есть версии для других систем, но в каком они сейчас состоянии я не знаю.
Или используем установленную в системе (должна работать 2.4.4, может 2.6, с 3.2 не работает).

```shell
$ wget http://ftp.kolibrios.org/users/bw/fpc/bootstrap-2.4.4-x86_64-linux.tar.bz2
$ tar -xz bootstrap-2.4.4-x86_64-linux.tar.bz2
```

Для ориентации в пространстве: вы уже скачали и распаковали `fpckos-2.4.4` в рабочую директорию,
в которой и находитесь.

```shell
$ ls
bootstrap-2.4.4/
bootstrap-2.4.4-x86_64-linux.tar.bz2
fpckos-2.4.4/
```

Сейчас нужно собрать нативную версию `fpcmake` с поддержкой KolibriOS.

```shell
$ bootstrap-2.4.4/x86_64-linux/fpcmake -Tall fpckos-2.4.4/utils/fpcm/Makefile.fpc
Processing fpckos-2.4.4/utils/fpcm/Makefile.fpc
 i386-linux requires: rtl,fcl-base
  ...
 Writing Makefile

$ make -C fpckos-2.4.4/utils/fpcm FPC=$PWD/bootstrap-2.4.4/x86_64-linux/ppcx64 UNITDIR=$PWD/bootstrap-2.4.4/x86_64-linux/units
$ alias fpcmake=$PWD/fpckos-2.4.4/utils/fpcm/fpcmake
```

Я сделал `alias`, вы можете `PATH` подправить, выполнить `make install` или ещё тысячу вещей с новым бинарником.
Дальше используется именно эта сборка `fpcmake`, а не из bootstrap-а.

Теперь собираем компилятор и необязательный утиль за компанию. Всё это хозяйство будет нативным (x86_64-linux в моём случае),
но компилятор уже сможет в i386-kolibri.

```shell
$ fpcmake -Tall fpckos-2.4.4/compiler/Makefile.fpc
$ fpcmake -Tall fpckos-2.4.4/compiler/utils/Makefile.fpc

$ make -C fpckos-2.4.4/compiler PPC_TARGET=i386 FPC=$PWD/bootstrap-2.4.4/x86_64-linux/ppcx64 UNITDIR=$PWD/bootstrap-2.4.4/x86_64-linux/units
```

Обратите внимание на `PPC_TARGET=i386`, мы не просто компилятор собираем, а кросс-компилятор. Т.е. это будет нативная штука,
для сборки i386 (т.е. x86, а не x86_64) кода: `ppc386`.

```shell
$ find fpckos-2.4.4 -type f -executable | xargs file | grep ELF | cut -d':' -f1
fpckos-2.4.4/compiler/utils/fpc
fpckos-2.4.4/compiler/utils/ppufiles
fpckos-2.4.4/compiler/utils/ppumove
fpckos-2.4.4/compiler/utils/mkarmins
fpckos-2.4.4/compiler/utils/fpcsubst
fpckos-2.4.4/compiler/utils/mkx86ins
fpckos-2.4.4/compiler/utils/ppudump
fpckos-2.4.4/compiler/ppc386
fpckos-2.4.4/utils/fpcm/fpcmake
```

Всё, bootstrap нам больше не нужен. И мы умеем собирать KEX-ы.

Собираем RTL.

```shell
$ fpcmake -Tall fpckos-2.4.4/rtl/Makefile.fpc
$ fpcmake -Tall fpckos-2.4.4/rtl/*/Makefile.fpc

$ make -C fpckos-2.4.4/rtl OS_TARGET=kolibri FPC=$PWD/fpckos-2.4.4/compiler/ppc386
```

@note: при указании FPC, CPU_TARGET не обязателен
@note: FPC можно объявить переменной окружения и не указывать каждый раз для make
@note: PREFIX также можно объявить переменной окружения и не указывать каждый раз для make

```shell
$ export FPCDIR=$PWD/fpckos-2.4.4
$ export FPC=$FPCDIR/compiler/ppc386
$ export PREFIX=$PWD/build/fpckos-2.4.4
```

Собственно это всё.
Можно бы пособирать "packages", но сейчас там толком ничего нет: `fcl-base` не портирован, он нужен был для нативных
сборок выше, а `fcl-process` хоть и компилируется, но в каком он состоянии я не знаю. Хотя хрен с ним, давайте соберём.

В качестве примера `Makefile` будет сделан только для `i386-kolibri`.

```shell
$ fpcmake -Ti386-kolibri fpckos-2.4.4/packages/fcl-process/Makefile.fpc
Processing fpckos-2.4.4/packages/fcl-process/Makefile.fpc
 i386-kolibri requires: rtl
 Writing Makefile

$ make -C fpckos-2.4.4/packages/fcl-process OS_TARGET=kolibri
```

Ещё раз обращу внимание на то что наш `ppc386` хоть и может собирать под Linux, но только под i386 и ему потребуется
дополнительный toolchain для этого, например `i386-linux-ld`, вместо `ld`.


Сборка KEX-ов
-------------

```
$ make -C fpckos-2.4.4/compiler OS_TARGET=kolibri
...
./ppc386 -Tkolibri -Pi386 -XPi386-kolibri- -Xr -Fui386 -Fusystems -Fu../rtl/units/i386-kolibri -Fii386 -FE.
         -FUi386/units/i386-kolibri -di386 -dGDB -dBROWSERLOG -Fux86 pp.pas
agx86att.pas(256,45) Warning: range check error while evaluating constants
agx86att.pas(246,16) Warning: unreachable code
pp.pas(224,1) Warning: Not supported section ".idata", objects 1, size 20
...
```

@see: .idata -> ogkex.pas

```
$ fpcmake -Tall fpckos-2.4.4/utils/Makefile.fpc
$ fpcmake -Tall fpckos-2.4.4/utils/*/Makefile.fpc

$ make -C fpckos-2.4.4/utils OS_TARGET=kolibri

$ find fpckos-2.4.4 -type f -name "*.kex"
fpckos-2.4.4/compiler/utils/fpc.kex
fpckos-2.4.4/compiler/utils/ppumove.kex
fpckos-2.4.4/compiler/utils/ppudump.kex
fpckos-2.4.4/compiler/utils/mkx86ins.kex
fpckos-2.4.4/compiler/utils/ppufiles.kex
fpckos-2.4.4/compiler/utils/mkarmins.kex
fpckos-2.4.4/compiler/utils/fpcsubst.kex
fpckos-2.4.4/compiler/ppc386.kex
fpckos-2.4.4/utils/data2inc.kex
fpckos-2.4.4/utils/fpcm/fpcmake.kex
```


Установка
---------

Шаг не обязательный, но так, например, можно перекинуть бинарники на диск с KolibriOS.
Смотрите пример с LRL, там я самую малось заморочился с установкой, может пригодится.

```
$ make install -C fpckos-2.4.4/compiler OS_TARGET=kolibri
$ make install -C fpckos-2.4.4/rtl/kolibri

$ make install -C fpckos-2.4.4/packages/fcl-process OS_TARGET=kolibri
__missing_command_FPCMAKE -p -Ti386-kolibri Makefile.fpc
```

Облом, бывает. Тогда мы так:

```
$ export FPCMAKE=$PWD/fpckos-2.4.4/utils/fpcm/fpcmake
$ make install -C fpckos-2.4.4/packages/fcl-process OS_TARGET=kolibri
```

@note:
  Результат будет разным в следующих двух случаях, предпочтение нужно отдавать первому варианту
  (не забудьте выполнить "unset FPC"):

  ```
  $ make install -C fpckos-2.4.4/compiler/utils FPC=$PWD/fpckos-2.4.4/compiler/ppc386 OS_TARGET=kolibri
  ```
  vs
  ```
  $ make install -C fpckos-2.4.4/compiler/utils CPU_TARGET=i386 OS_TARGET=kolibri
  ```

  То же самое для:

  ```
  $ make kolibri_install -C rtl FPC=$PWD/fpckos-2.4.4/compiler/ppc386 OS_TARGET=kolibri
  ```
  vs
  ```
  $ make kolibri_install -C rtl CPU_TARGET=i386 OS_TARGET=kolibri
  ```

  Просто на заметку.


Чистка исходников
-----------------

```
$ make cleanall -C fpckos-2.4.4/rtl OS_TARGET=kolibri
$ make cleanall -C fpckos-2.4.4/packages/fcl-process OS_TARGET=kolibri

$ make cleanall -C fpckos-2.4.4/utils OS_TARGET=kolibri
$ make cleanall -C fpckos-2.4.4/utils

$ make cleanall -C fpckos-2.4.4/compiler OS_TARGET=kolibri
$ make clean -C fpckos-2.4.4/compiler OS_TARGET=kolibri

$ make cleanall -C fpckos-2.4.4/compiler
$ make clean -C fpckos-2.4.4/compiler
```

Makefile-ы остаются, ну в принципе и фиг с ними.

Другий вариант чистки, с удалением всех Makefile-ов:

```
$ find $FPCDIR -type f -name Makefile -delete
$ find $FPCDIR -type f -name "*.i386-kolibri" -delete
$ find $FPCDIR -type f -name "*.x86_64-linux" -delete
$ find $FPCDIR -type f -name "*.kex" -delete
$ find $FPCDIR -type f -executable | xargs file | grep ELF | cut -d':' -f1 | xargs rm
$ find $FPCDIR -type d -name units | xargs rm -rf
```


Сборка примеров
---------------

Примеры не включены в архив с портом FPC.
Состояние на данный момент: всё собрано как выше, шаг с чисткой пропущен, хе-хе :-).
В `fpcdocs` примеры из официальной документации. Эти примеры совершенно в удручающем состоянии
и многие не работают из коробки (на любой платформе), но на безрыбье - и так сойдёт.

```
$ ls
bootstrap-2.4.4/
bootstrap-2.4.4-x86_64-linux.tar.bz2
build/
examples/
fpckos-2.4.4/

$ fpcmake -Ti386-kolibri -r examples/Makefile.fpc
Processing examples/Makefile.fpc
 i386-kolibri requires: rtl
 Writing Makefile
Processing examples/fpcdocs/Makefile.fpc
 i386-kolibri requires:
 Writing Makefile
Processing examples/fpcdocs/heapex/Makefile.fpc
 i386-kolibri requires: rtl
 Writing Makefile
...

$ make -C examples
```

------

```
$ ls
bootstrap-2.4.4/
bootstrap-2.4.4-x86_64-linux.tar.bz2
build/
examples/
fpckos-2.4.4/

$ fpcmake -Ti386-kolibri examples/Makefile.fpc
$ make -C examples
```


Сборка LRL (Lode Runer LIVE)
----------------------------

LRL не включён в архив с портом FPC.

LRL это древняя OpenSource игра, написанная на Pascal под DOS. Автор Aleksey V. Vaneev.
Я выполнил её портирование на KolibriOS.

Переменные окружения FPC, FPCDIR и PREFIX должны быть предварительно настроены.

```
$ ls
apps/
bootstrap-2.4.4/
bootstrap-2.4.4-x86_64-linux.tar.bz2
build/
examples/
fpckos-2.4.4/

$ fpcmake -Ti386-kolibri apps/lrl/Makefile.fpc
Processing examples/Makefile.fpc
 i386-kolibri requires: rtl
 Writing Makefile

$ make -C apps/lrl
$ make install -C apps/lrl PREFIX=$PWD/build/lrl
```
