{$codepage utf8}
{$mode objfpc}

unit ogkex;


interface


uses
  cclasses, ogbase, ogcoff;


type
  TKolibriExeOutput = class(TPECoffExeOutput)
  public
    procedure GenerateLibraryImports(ImportLibraryList: TFPHashObjectList); override;
    function WriteData: Boolean; override;
  end;


implementation


uses
  SysUtils, Math,
  verbose, globals;


{constructor TPECoffexeoutput.create;
begin
  inherited createcoff(true);
  CExeSection:=TPECoffExeSection;
  CObjData := TPECoffObjData; !!!
end;}


procedure TKolibriExeOutput.GenerateLibraryImports(ImportLibraryList: TFPHashObjectList);
begin
  if 0 < ImportLibraryList.Count then
    InternalError(2011102202);
end;

function TKolibriExeOutput.WriteData: Boolean;

type
  TKosSign = array[0..7] of Byte;
  TKosHeader = packed record
    sign   : TKOSSign;
    version: DWord;
    start  : DWord;
    size   : DWord;
    memory : DWord;
    stack  : DWord;
    args   : DWord;
    path   : DWord;
  end;

const
  KOS_SIGN: TKOSSign = ($4D, $45, $4E, $55, $45, $54, $30, $31);
  ARGS_SIZE = 512;
  PATH_SIZE = 512;

var
  KosHeader: TKosHeader;
  I, J, K  : Integer;
  TmpBuffer: array[0..31] of Char;
  TmpHex   : String;
  TmpI, TmpSize, TmpPos: DWord;

begin
  Comment(V_Debug, '[TKolibriExeOutput.WriteData]');

  with KosHeader do
  begin
    sign    := KOS_SIGN;
    version := 1;
    start   := ImageBase + EntrySym.Address;
    size    := 0;
  end;

  for I := 0 to ExeSectionList.Count - 1 do
  with TExeSection(ExeSectionList[I]) do
    KosHeader.size := Max(KosHeader.size, MemPos + Size);

  with KosHeader do
  begin
    args    := size;
    path    := args + ARGS_SIZE;
    stack   := path + PATH_SIZE + StackSize;
    memory  := stack;
  end;

  FWriter.Write(KosHeader, SizeOf(KosHeader));

  for I := 0 to ExeSectionList.Count - 1 do
  with TExeSection(ExeSectionList[I]) do
  begin
    if MemPos < FWriter.Size then
      InternalError(2011102203);

    Comment(V_Debug, Format('* Section "%s": objects %d, size %d, mempos 0x%s', [Name, ObjSectionList.Count, Size, IntToHex(MemPos, 8)]));

    if (Name <> '.text') and (Name <> '.data') and (Name <> '.rsrc') and (Name <> '.bss') then
      Comment(V_Warning, Format('Not supported (unknown) section "%s", objects %d, size %d, mempos 0x%s',
        [Name, ObjSectionList.Count, Size, IntToHex(MemPos, 8)]));
      // @ex: .idata,   objects 1, size 20
      // @ex: .stab,    objects 1, size 1284
      // @ex: .stabstr, objects 1, size 17512

    if 0 < ObjSectionList.Count then
    begin
      while FWriter.Size < MemPos do
        FWriter.WriteZeros(Min(1024, MemPos - FWriter.Size));

      for J := 0 to ObjSectionList.Count - 1 do
      with TObjSection(ObjSectionList[J]) do
      begin
        if MemPos < FWriter.Size then
          InternalError(2011102205);

        while FWriter.Size < MemPos do
          FWriter.WriteZeros(Min(1024, MemPos - FWriter.Size));

        if Assigned(Data) then
        begin
          Comment(V_Debug, Format('  Object "%s": size %d, mempos 0x%s, datapos 0x%s', [FullName, Size,
            IntToHex(MemPos, 8), IntToHex(DataPos, 8)]));
          if (Name = '.stab') or (Name = '.stabstr') then
          begin
            FillChar(TmpBuffer, SizeOf(TmpBuffer), #0);
            TmpPos  := Data.Pos;
            Data.Seek(0);
            TmpSize := Data.Read(TmpBuffer, SizeOf(TmpBuffer));
            Data.Seek(TmpPos);
            TmpHex := '';
            for TmpI := 0 to TmpSize - 1 do
            begin
              TmpHex := TmpHex + IntToHex(Byte(TmpBuffer[TmpI]), 2);
              if TmpI < TmpSize - 1 then
                TmpHex := TmpHex + ' ';
            end;
            Comment(V_Debug, Format('  (%d) [%s]', [TmpSize, TmpHex]));
           {FWriter.Write('>>>>>>>>', 8);}
          end;
          FWriter.WriteArray(Data);
        end else
        while FWriter.Size < MemPos + Size do
          FWriter.WriteZeros(Min(1024, MemPos + Size - FWriter.Size));
      end;
    end;
  end;

  if KosHeader.size <> FWriter.Size then
  begin
    Comment(V_Fatal, Format('Expected file size %d, but got %d', [KosHeader.size, FWriter.Size]));
    InternalError(200602252);
  end;

  { @todo: symbols, see ogcoff writedata }

  Result := True;
end;


end.
