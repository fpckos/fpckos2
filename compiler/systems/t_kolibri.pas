
unit t_kolibri;

{$i fpcdefs.inc}


interface


uses
  cutils,cclasses,
  aasmbase,aasmtai,aasmdata,aasmcpu,fmodule,globtype,globals,systems,verbose,
  symconst,symdef,symsym,
  script,gendef,
  cpubase,
  import, export, link, comprsrc, cgobj,
  {ppu, -- uf_has_resourcestrings}
  i_kolibri,
  t_win;


type
  TLinkerKolibri = class(TInternalLinker)
    constructor Create; override;
    procedure DefaultLinkScript; override;
    procedure InitSysInitUnitName; override;
  end;

  TImportLibKolibri = class(TImportLibWin);
  TExportLibKolibri = class(TExportLibWin);


implementation


uses
  SysUtils,
  cfileutl,
  cpuinfo, cgutils, dbgbase,
  owar, ogbase, ogcoff, ogkex;


{$ifdef NOTARGETWIN}
const
  res_gnu_windres_info: TResInfo = (
    id      : res_gnu_windres;
    resbin  : 'fpcres';
    rescmd  : '-o $OBJ -a $ARCH -of coff $DBG';
    rcbin   : 'windres';
    rccmd   : '--include $INC -O res -o $RES $RC';
    resourcefileclass : nil;
    resflags: [];
  );
{-$else see t_win.pas}
{$endif}

constructor TLinkerKolibri.Create;
begin
  inherited Create;
  CExeOutput := TKolibriExeOutput;
  CObjInput  := TPECoffObjInput;
end;

procedure TLinkerKolibri.DefaultLinkScript;
var
  s, s2   : TCmdStr;
  secname : String;
  secnames: String;
begin
  with LinkScript do
  begin
    while not ObjectFiles.Empty do
    begin
      s := ObjectFiles.GetFirst;
      if s <> '' then
        Concat('READOBJECT ' + MaybeQuoted(s));
    end;

    while not StaticLibFiles.Empty do
    begin
      s := StaticLibFiles.GetFirst;
      if s <> '' then
        Concat('READSTATICLIBRARY ' + MaybeQuoted(s));
    end;

    while not SharedLibFiles.Empty do
    begin
      s := SharedLibFiles.GetFirst;
      if FindLibraryFile(s, target_info.staticClibprefix, target_info.staticClibext, s2) then
        Concat('READSTATICLIBRARY ' + MaybeQuoted(s2)) else
        Comment(V_Error, 'Import library not found for ' + s);
    end;

    if IsSharedLibrary then
        InternalError(2011102201) else
    if apptype = app_gui then
        Concat('ENTRYNAME _startGUI') else
        Concat('ENTRYNAME _startConsole');

    if ImageBaseSetExplicity then
      InternalError(2011102202);

    ImageBase := 0;

    { [bw] это копипаста из t_win.pas и я не разбирался (по большей части), что тут и для чего нужно }

    Concat('IMAGEBASE $' + HexStr(ImageBase, SizeOf(ImageBase) * 2));
    Concat('HEADER');
{   Concat('EXESECTION .fpckoshdr');
    Concat('  LONG 0');
    Concat('  LONG 1');
    Concat('  LONG 2');
    Concat('  LONG 3');
    Concat('ENDEXESECTION');}
    Concat('EXESECTION .text');
    Concat('  SYMBOL __text_start__');
    Concat('  OBJSECTION .text*');
    Concat('  SYMBOL ___CTOR_LIST__');
    Concat('  SYMBOL __CTOR_LIST__');
    Concat('  LONG -1');
    Concat('  OBJSECTION .ctor*');
    Concat('  LONG 0');
    Concat('  SYMBOL ___DTOR_LIST__');
    Concat('  SYMBOL __DTOR_LIST__');
    Concat('  LONG -1');
    Concat('  OBJSECTION .dtor*');
    Concat('  LONG 0');
    Concat('  SYMBOL etext');
    Concat('ENDEXESECTION');
    Concat('EXESECTION .data');
    Concat('  SYMBOL __data_start__');
    Concat('  OBJSECTION .data*');
    Concat('  OBJSECTION .fpc*');
    Concat('  SYMBOL edata');
    Concat('  SYMBOL __data_end__');
    Concat('ENDEXESECTION');
    Concat('EXESECTION .rdata');
    Concat('  SYMBOL ___RUNTIME_PSEUDO_RELOC_LIST__');
    Concat('  SYMBOL __RUNTIME_PSEUDO_RELOC_LIST__');
    Concat('  OBJSECTION .rdata_runtime_pseudo_reloc');
    Concat('  SYMBOL ___RUNTIME_PSEUDO_RELOC_LIST_END__');
    Concat('  SYMBOL __RUNTIME_PSEUDO_RELOC_LIST_END__');
    Concat('  OBJSECTION .rdata*');
    Concat('  OBJSECTION .rodata*');
    Concat('ENDEXESECTION');
    Concat('EXESECTION .pdata');
    Concat('  OBJSECTION .pdata');
    Concat('ENDEXESECTION');
    Concat('EXESECTION .bss');
    Concat('  SYMBOL __bss_start__');
    Concat('  OBJSECTION .bss*');
    Concat('  SYMBOL __bss_end__');
    Concat('ENDEXESECTION');
    Concat('EXESECTION .idata');
    Concat('  OBJSECTION .idata$2*');
    Concat('  OBJSECTION .idata$3*');
    Concat('  ZEROS 20');
    Concat('  OBJSECTION .idata$4*');
    Concat('  OBJSECTION .idata$5*');
    Concat('  OBJSECTION .idata$6*');
    Concat('  OBJSECTION .idata$7*');
    Concat('ENDEXESECTION');

    Concat('EXESECTION .rsrc');
    Concat('  SYMBOL __rsrc_start__');
    Concat('  SYMBOL FPC_RESSYMBOL');
    Concat('  OBJSECTION .rsrc*');
    Concat('ENDEXESECTION');

    secnames:='.edata,.reloc,.gnu_debuglink,' +
              '.debug_aranges,.debug_pubnames,.debug_info,.debug_abbrev,.debug_line,.debug_frame,.debug_str,.debug_loc,' +
              '.debug_macinfo,.debug_weaknames,.debug_funcnames,.debug_typenames,.debug_varnames,.debug_ranges';
    while secnames <> '' do
    begin
      secname := GetToken(secnames, ',');
      Concat('EXESECTION ' + secname);
      Concat('  OBJSECTION ' + secname + '*');
      Concat('ENDEXESECTION');
    end;

    { Can't use the generic rules, because that will add also .stabstr to .stab }
    Concat('EXESECTION .stab');
    Concat('  SYMBOL __stab');
    Concat('  OBJSECTION .stab');
    Concat('ENDEXESECTION');
    Concat('EXESECTION .stabstr');
    Concat('  SYMBOL __stabstr');
    Concat('  OBJSECTION .stabstr');
    Concat('ENDEXESECTION');
    Concat('STABS');
    Concat('SYMBOLS');
  end;
end;

procedure TLinkerKolibri.InitSysInitUnitName;

  procedure ShowLinkedList(Name: String; List: TLinkedList);
  begin
    WriteLn('  ' + Name + ': ', List.Count);
  end;

var
  hp: TModule;
begin
  if AppType = app_gui then
    SysInitUnit := 'sysinitgui' else
    SysInitUnit := 'sysinitconsole';

  if CheckVerbosity(V_Debug) then
  begin
    WriteLn('[TLinkerKolibri.InitSysInitUnitName]');

    hp := TModule(loaded_units.first);

    WriteLn('Loaded Units:');

    while Assigned(hp) do
    begin
      WriteLn('* ModuleName: ', hp.ModuleName^);
      WriteLn('  Flags     : ', hp.Flags);
      if Assigned(hp.LocalSymTable.Name) then
        WriteLn('  LocalSymTable: ', hp.LocalSymTable.Name^) else {RealName}
        WriteLn('  LocalSymTable: <nil>');
      ShowLinkedList('ResourceFiles',       hp.ResourceFiles);
      ShowLinkedList('LinkUnitStaticLibs',  hp.LinkUnitStaticLibs);
      //ShowLinkedList('LinkOtherSharedLibs', hp.LinkOtherSharedLibs);
      ShowLinkedList('LinkOtherStaticLibs', hp.LinkOtherStaticLibs);
      // if (hp.flags and uf_has_resourcestrings) = uf_has_resourcestrings then ...
      hp := TModule(hp.next);
    end;
  end;
end;


initialization
{$ifdef i386}
  RegisterInternalLinker(system_i386_kolibri_info, TLinkerKolibri);
  RegisterImport(system_i386_kolibri, TImportLibKolibri);
  RegisterExport(system_i386_kolibri, TExportLibKolibri);
  RegisterTarget(system_i386_kolibri_info);
{$ifdef NOTARGETWIN}
  RegisterRes(res_gnu_windres_info, TWinLikeResourceFile);
{$endif}
{$endif i386}
end.
