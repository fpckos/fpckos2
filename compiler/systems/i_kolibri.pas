
unit i_kolibri;

{$i fpcdefs.inc}


interface


uses
  systems;


const
  system_i386_kolibri_info: TSystemInfo = (
    system   : system_i386_kolibri;
    name     : 'Kolibri for i386';
    shortname: 'Kolibri';
    flags    : [
      tf_under_development,
      tf_files_case_aware, tf_use_function_relative_addresses,
      tf_smartlink_library, tf_smartlink_sections, tf_no_pic_supported,
      tf_has_winlike_resources];
    cpu         : cpu_i386;
    unit_env    : 'KOLIBRIUNITS';
    extradefines: 'KOLIBRI;KOLIBRI32;KOS;KOS32';
    exeext      : '.kex';
    defext      : '.def';
    scriptext   : '.sh';
    smartext    : '.sl';
    unitext     : '.ppu';
    unitlibext  : '.ppl';
    asmext      : '.s';
    objext      : '.o';
    resext      : '.res';
    resobjext   : '.or';
    sharedlibext: '.dll';
    staticlibext: '.a';
    staticlibprefix : 'libp';
    sharedlibprefix : '';
    sharedClibext   : '.dll';
    staticClibext   : '.a';
    staticClibprefix: 'lib';
    sharedClibprefix: '';
    importlibprefix : 'libimp';
    importlibext    : '.a';
    Cprefix         : '_';
    newline      : #13#10;
    dirsep       : '/';
    assem        : as_i386_pecoff;
    assemextern  : as_gas;
    link         : nil;
    linkextern   : nil;
    ar           : ar_gnu_ar;

// @todo: [bw] (?) i386-kolibri-windres
//  res          : res_none;
    res          : res_gnu_windres;
//  res          : res_elf;

    dbg          : dbg_stabs;
    script       : script_unix;
    endian       : endian_little;
    alignment    : (
      procalign      : 16;
      loopalign      : 4;
      jumpalign      : 0;
      constalignmin  : 0;
      constalignmax  : 16;
      varalignmin    : 0;
      varalignmax    : 16;
      localalignmin  : 4;
      localalignmax  : 8;
      recordalignmin : 0;
      recordalignmax : 4;
      maxCrecordalign: 16);
    first_parm_offset: 8;
    stacksize: 262144;
    abi      : abi_default);


implementation


initialization
{$ifdef CPU86}
{$ifdef kolibri}
  set_source_info(system_i386_kolibri_info);
{$endif kolibri}
{$endif CPU86}
end.
