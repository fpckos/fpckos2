{$codepage utf8}
{$mode objfpc}

unit Classes;

interface

uses
  SysUtils, Types, TypInfo, RtlConsts;

{$i classesh.inc}

implementation

{$i classes.inc}

initialization
//WrapperBlockList:=nil;    <-- win32 !
//TrampolineFreeList:=nil;  <-- win32 !
//InitCriticalSection(CritObjectInstance);  <-- win32 !
  CommonInit;
finalization
  CommonCleanup;  // <- unix !
//if ThreadsInited then DoneThreads;  <-- unix
//DeleteInstBlockList;  <-- win32 !
//DoneCriticalSection(CritObjectInstance);  <-- win32 !
end.
