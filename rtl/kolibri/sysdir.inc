{TODO}

procedure mkdir(s: PChar; len: SizeUInt); [IOCheck, public, alias: 'FPC_SYS_MKDIR'];
begin
  InOutRes := 211;
end;

procedure rmdir(s: PChar; len: SizeUInt); [IOCheck, public, alias: 'FPC_SYS_RMDIR'];
begin
  InOutRes := 211;
end;

procedure chdir(s: PChar; len: SizeUInt); [IOCheck, public, alias: 'FPC_SYS_CHDIR'];
var
  Path: AnsiString;
begin
  Path := s;

  while (0 < Length(Path)) and (Path[Length(Path)] in AllowDirectorySeparators) do
    SetLength(Path, Length(Path) - 1);

  if 0 < Length(Path) then
  begin
    kos_setdir(PChar(Path));
    InOutRes := 0;
    Exit;
  end;

  InOutRes := 3;
end;

procedure getdir(DriveNr: Byte; var Dir: ShortString);
{ DriveNr не используется, но всегда должен быть равен 0 }
var
  Path: array[Byte] of Char;
begin
  if DriveNr <> 0 then
    InOutRes := 15 { Invalid drive number (неправильный номер устройства) }  else
  begin
    kos_getdir(@Path, SizeOf(Path));
    Dir := StrPas(Path);
    InOutRes := 0;
  end;
end;
