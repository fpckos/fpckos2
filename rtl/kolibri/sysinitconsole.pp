{$codepage utf8}
{$mode objfpc}

unit sysinitconsole;

interface

implementation

{$i kos_term.inc}
{$i sysinitkos.inc}


procedure StartConsole; assembler; nostackframe; public name '_startConsole';
asm
  movb $1, IsConsole
  jmp StartProgram
end;

procedure SetupKonsole;
var
  TermPidS: String;
  TermPid : TThreadID;
  Code    : SizeInt;
begin
  TermPidS := GetKosXpcData('terminal.pid');

  if TermPidS <> '' then
  begin
    Val(TermPidS, TermPid, Code);
    if Code <> 0 then Halt(255);
    KonsoleStartSlave(TermPid);
  end else
    KonsoleStartMaster;

  KonsoleAssignStdIO;
end;

initialization
  { Здесь следовало бы сделать регистрацию доступной реализации терминала `Konsole`, а не её/его запуск.
    А в качестве альтернативы могла бы выступать (отсутствующая) реализация в `Crt`.
    Окончательное решение могло бы приниматься в `system.pp/FinalInit`.
    До этого момента весь вывод следовало бы буферизовать, а при попытве ввода - завершаться с ошибкой.
    Но я устал, я мухожук. }
  if IsConsole then
    SetupKonsole;
end.
