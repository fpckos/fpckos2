{$codepage utf8}
{$mode objfpc}
{$h+}

unit SysUtils;


interface


{$define HAS_SLEEP} {???}
{-$DEFINE HAS_OSERROR}
{-$DEFINE HAS_OSCONFIG}
{-$DEFINE HAS_CREATEGUID}


{ Include platform independent interface part }
{$i sysutilh.inc}


implementation


uses
  SysConst;


{-$define HASCREATEGUID}
{-$define HASEXPANDUNCFILENAME}
{-$define FPC_NOGENERICANSIROUTINES}
{-$define FPC_FEXPAND_UNC} (* UNC paths are supported *)
{-$define FPC_FEXPAND_DRIVES} (* Full paths begin with drive specification *)

{ Include platform independent implementation part }
{$i sysutils.inc}


{****************************************************************************
                              File Functions
****************************************************************************}

function KosToWinAge(Time, Date: DWord): Longint;
begin
  Result := DateTimeToFileDate(
    EncodeDate((Date shr 16) and $FFFF, (Date shr 8) and $FF, Date and $FF) +
    EncodeTime((Time shr 16) and $FF, (Time shr 8) and $FF, Time and $FF, 0));
//WriteLn('..KosToWinAge :: ', Time, ',', Date, ' -> ', Result);
  { 604728,131795481 -> 1062817628 }
  { x93a38, x7db0a19 ->  x3f594f5c }
end;


const
  FILEHANDLEPREFIX = $4000;
type
  PFileRecord = ^TFileRecord;
  TFileRecord = record
    InUse: Boolean;
    F: File;
  end;
var
  FileHandles: array of TFileRecord;

function FileRecordByHandle(Handle: THandle): PFileRecord;
begin
  Dec(Handle, FILEHANDLEPREFIX);
  Result := @FileHandles[Handle];
end;

function CreateFileRecord(): THandle;
var
  I, C: Longword;
begin
  Result := -1;
  C := Length(FileHandles);
  for I := 0 to C - 1 do
  if not FileHandles[I].InUse then
  begin
    Result := I;
    Break;
  end;
  if Result < 0 then
  begin
    SetLength(FileHandles, C + 1);
    Result := C;
  end;
  FileHandles[Result].InUse := True;
  FillChar(FileHandles[Result].F, SizeOf(FileRec), 0);
  Inc(Result, FILEHANDLEPREFIX);
end;

procedure ReleaseFileRecord(Handle: THandle);
begin
  FileRecordByHandle(Handle)^.InUse := False;
end;

function FileOpen(const FileName: String; Mode: Integer): THandle;
var
  F: File;
begin
  Filemode := Mode;  { @xxx: thread unsafe }
  Assign(F, FileName);
{$ifopt i+}
  {$i-}
  Reset(F, 1);
  {$i+}
{$else}
  Reset(F, 1);
{$endif}
  if InOutRes = 0 then
  begin
    Result := CreateFileRecord();
    FileRecordByHandle(Result)^.F := F;
  end else
  begin
    InOutRes := 0;
    Result := feInvalidHandle;
  end;
end;

function FileCreate(const FileName: String): THandle;
var
  F: File;
begin
  Assign(F, FileName);
{$ifopt i+}
  {$i-}
  Rewrite(F, 1);
  {$i+}
{$else}
  Rewrite(F, 1);
{$endif}
  if InOutRes = 0 then
  begin
    Result := CreateFileRecord();
    FileRecordByHandle(Result)^.F := F;
  end else
  begin
    InOutRes := 0;
    Result := feInvalidHandle;
  end;
end;

function FileCreate(const FileName: String; Mode: Integer): THandle;
var
  F: File;
begin
  Filemode := Mode;  { @xxx: thread unsafe }
  Assign(F, FileName);
{$ifopt i+}
  {$i-}
  Rewrite(F, 1);
  {$i+}
{$else}
  Rewrite(F, 1);
{$endif}
  if InOutRes = 0 then
  begin
    Result := CreateFileRecord();
    FileRecordByHandle(Result)^.F := F;
  end else
  begin
    InOutRes := 0;
    Result := feInvalidHandle;
  end;
end;

function FileRead(Handle: THandle; out Buffer; Count: Longint): Longint;
begin
  BlockRead(FileRecordByHandle(Handle)^.F, Buffer, Count, Result);
end;

function FileWrite(Handle: THandle; const Buffer; Count: Longint): Longint;
begin
  BlockWrite(FileRecordByHandle(Handle)^.F, Buffer, Count, Result);
end;

function FileSeek(Handle: THandle; FOffset, Origin: Longint): Longint;
begin
  Result := FileSeek(Handle, Int64(FOffset), Origin);
end;

function FileSeek(Handle: THandle; FOffset: Int64; Origin: Longint): Int64;
var
  Position: Int64;
begin
  case Origin of
    fsFromBeginning: Position := FOffset;
    fsFromCurrent: Position := FilePos(FileRecordByHandle(Handle)^.F) + FOffset;
    fsFromEnd: Position := FileSize(FileRecordByHandle(Handle)^.F) + FOffset;
  end;
  {TODO: проверка соответствия [0..filesize]}
  Seek(FileRecordByHandle(Handle)^.F, Position);
  Result := Position;
end;

procedure FileClose(Handle: THandle);
begin
  Close(FileRecordByHandle(Handle)^.F);
  ReleaseFileRecord(Handle);
end;

function FileTruncate(Handle: THandle; Size: Int64): Boolean;
begin
  Result := False;
end;

function FileAge(const FileName: String): Longint;
var
  Handle: THandle;
begin
  Handle := FileOpen(FileName, fmOpenRead);
  if Handle <> feInvalidHandle then
  begin
    Result := FileGetDate(Handle);
    FileClose(Handle);
  end else
    Result := -1;
end;

function FileExists(const FileName: String): Boolean;
var
  F: File;
begin
  Assign(F, FileName);
{$ifopt i+}
  {$i-}
  Reset(F, 1);
  {$i+}
{$else}
  Reset(F, 1);
{$endif}
  if InOutRes = 0 then
  begin
    Close(F);
    Result := True;
  end else
  begin
    InOutRes := 0;
    Result := False;
  end;
end;

function DirectoryExists(const Directory: String): Boolean;
begin
  Result := False;
end;


{ == Поиск ==== }

const
  FINDHANDLEPREFIX = $8000;
  FindReadDirLimit = 10;
  FindReadDirSize  = SizeOf(TKosReadDir) + FindReadDirLimit * SizeOf(TKosAsciiBDFE);

type
  PFindRecord = ^TFindRecord;
  TFindRecord = record
    Used    : Boolean;
    BasePath: String;
    FileMask: String;
    Index   : Integer;
    ReadedTotal: Longword;
    ReadDir : array[0..FindReadDirSize - 1] of Byte;
  end;

var
  FindRecords: array of TFindRecord;


function FindRecordByHandle(Handle: Longint): PFindRecord;
begin
  Dec(Handle, FINDHANDLEPREFIX);
  if (Low(FindRecords) <= Handle) and (Handle <= High(FindRecords)) and FindRecords[Handle].Used then
    Result := @FindRecords[Handle] else
    Result := nil;
end;


function CreateFindRecord: Longint;
var
  I: Longint;

begin
  Result := -1;

  for I := Low(FindRecords) to High(FindRecords) do
  if not FindRecords[I].Used then
  begin
    Result := I;
    Break;
  end;

  if Result < 0 then
  begin
    Result := Length(FindRecords);
    SetLength(FindRecords, Result + 1);
  end;

  with FindRecords[Result] do
  begin
    Used     := True;
    BasePath := '';
    FileMask := '';
    Index    := -1;
    ReadedTotal := 0;
    FillChar(ReadDir, SizeOf(ReadDir), 0);
  end;

  Inc(Result, FINDHANDLEPREFIX);
end;


procedure ReleaseFindRecord(Handle: Longint);
var
  FindRecord: PFindRecord;
begin
  FindRecord := FindRecordByHandle(Handle);
  if Assigned(FindRecord) then
  with FindRecord^ do
  begin
    Used  := False;
    Index := -1;
  end;
end;


function FNMatch(Pattern, Name: String): Boolean;
var
  LenPat, LenName: Longint;

  function DoFNMatch(i, j: Longint): Boolean;
  var
    Found: Boolean;
  begin
    Found := true;
    while Found and (i <= LenPat) do
    begin
      case Pattern[i] of
        '?': Found := (j <= LenName);
        '*': begin
          {find the next character in pattern, different of ? and *}
          while Found do
          begin
            Inc(i);
            if i > LenPat then
              Break;
            case Pattern[i] of
              '*': ;
              '?': begin
                if j > LenName then
                begin
                  Result := False;
                  Exit;
                end;
                Inc(j);
              end;
              else
                Found := False;
            end;
          end;

          Assert((i > LenPat) or ((Pattern[i] <> '*') and (Pattern[i] <> '?')));

          { Now, find in name the character which i points to, if the * or ?
            wasn't the last character in the pattern, else, use up all the
            chars in name }

          Found := False;
          if (i <= LenPat) then
          begin
            repeat
              {find a letter (not only first !) which maches pattern[i]}
              while (j <= LenName) and (Name[j] <> Pattern[i]) do
                Inc(j);

              if (j < LenName) then
              begin
                if DoFnMatch(i + 1, j + 1) then
                begin
                  i := LenPat;
                  j := LenName;  {we can stop}
                  Found := True;
                  Break;
                end else
                  Inc(j);{We didn't find one, need to look further}
              end else
              if j = LenName then
              begin
                Found := True;
                Break;
              end;
              { This 'until' condition must be j>LenName, not j>=LenName.
                That's because when we 'need to look further' and
                j = LenName then loop must not terminate. }
            until (j > LenName);

          end else
          begin
            j := LenName;  {we can stop}
            Found := True;
          end;
        end;

    else {not a wildcard character in pattern}
      Found := (j <= LenName) and (Pattern[i] = Name[j]);
    end;

    Inc(i);
    Inc(j);
  end;
    Result := Found and (j > LenName);
  end;

begin
  Pattern := LowerCase(Pattern);
  Name    := LowerCase(Name);
  LenPat  := Length(Pattern);
  LenName := Length(Name);
  FNMatch := DoFNMatch(1, 1);
end;


function FindMatch(var F: TSearchRec): Longint;

  function ReadNext(FindRecord: PFindRecord): Boolean;
  var
    KosFile  : PKosFile;
    BasePath : AnsiString;
    ReadCount: Longint;
    ReadCode : Longint;
  begin
    BasePath := FindRecord^.BasePath + #0;
    KosFile  := GetMem(SizeOf(TKosFile));

    with KosFile^ do
    begin
      Position := FindRecord^.ReadedTotal;
      Size := FindReadDirLimit;
      Data := @FindRecord^.ReadDir;
      Name[0] := #0;
      PPointer(@Name[0] + 1)^ := @BasePath[1];
    end;

    ReadCode := kos_readdir(KosFile, ReadCount);
    if (ReadCode = 0) or (ReadCode = 6) then  { 6 == EOF }
    begin
      Inc(FindRecord^.ReadedTotal, ReadCount);
      FindRecord^.Index := 0;
      Result := ReadCount > 0;
    end else
    begin
      FindRecord^.Index := -1;
      Result := False;
    end;

    FreeMem(KosFile);
  end;

var
  FindRecord: PFindRecord;
  BDFE: PKosAsciiBDFE;

begin
  FindRecord := FindRecordByHandle(F.FindHandle);

  if not Assigned(FindRecord) then
  begin
    Result := -1;
    Exit;
  end;

  with FindRecord^ do
  begin
    if (FileMask = '') or ((Index >= 0) and (PKosReadDir(@ReadDir)^.ReadCount = 0)) then
    begin
      Result := -1;
      Exit;
    end;

    if (Index < 0) and not ReadNext(FindRecord) then
    begin
      Result := -1;
      Exit;
    end;

    Dec(Index);
    BDFE := PKosAsciiBDFE(PByte(@ReadDir) + SizeOf(TKosReadDir) + SizeOf(TKosAsciiBDFE) * Index);
    repeat
      Inc(Index);

      if Index < Integer(PKosReadDir(@ReadDir)^.ReadCount) then
        Inc(BDFE) else

      if ReadNext(FindRecord) then
        BDFE := PKosAsciiBDFE(PByte(@ReadDir) + SizeOf(TKosReadDir) + SizeOf(TKosAsciiBDFE) * Index) else
      begin
        Result := -1;
        Exit;
      end;

      with BDFE^ do
      begin
        F.Name := ExtractFileName(StrPas(Name));
        F.Attr := Attributes;
        F.Size := Size;

        if MDate <> 0 then
          F.Time := KosToWinAge(MTime, MDate) else
          F.Time := KosToWinAge(MTime, $076C0101);
      end;

    until ((F.Attr and F.ExcludeAttr) = 0) and FNMatch(FileMask, F.Name);
    Inc(Index);

    Result := 0;
  end;
end;

function FindFirst(const Path: String; Attr: Longint; out Rslt: TSearchRec): Longint;
var
  FindRecord: PFindRecord;
  FullPath  : String;
begin
  Rslt.Name := Path;
  Rslt.Attr := Attr;
  Rslt.ExcludeAttr := not Attr;
  Rslt.FindHandle  := CreateFindRecord();

  FullPath := ExpandFileName(Path);

  FindRecord := FindRecordByHandle(Rslt.FindHandle);
  with FindRecord^ do
  begin
    BasePath := ExtractFilePath(FullPath);
    FileMask := ExtractFileName(FullPath);
  end;

  Result := FindMatch(Rslt);
end;

function FindNext(var Rslt: TSearchRec): Longint;
begin
  Result := FindMatch(Rslt);
end;

procedure FindClose(var F: TSearchRec);
begin
  ReleaseFindRecord(F.FindHandle);
end;


function FileGetDate(Handle: THandle): Longint;
var
  BDFE: TKosAsciiBDFE;
  F : File;
  KF: PKosFile;
begin
  { @xxx: проверки handle'а: feInvalidHandle, ... }
  F  := FileRecordByHandle(Handle)^.F;
  KF := PKosFile(FileRec(F).Handle);
  KF^.Data := @BDFE;
  if kos_fileinfo(KF) = 0 then
    Result := KosToWinAge(BDFE.MTime, BDFE.MDate) else
    Result := -1;
end;

function FileSetDate(Handle: THandle; Age: Longint): Longint;
begin
  Result := feInvalidHandle;
end;

function FileGetAttr(const FileName: String): Longint;
begin
  Result := feInvalidHandle;
end;

function FileSetAttr(const Filename: String; Attr: longint): Longint;
begin
  Result := feInvalidHandle;
end;

function DeleteFile(const FileName: String): Boolean;
begin
  Result := False;
end;

function RenameFile(const OldName, NewName: String): Boolean;
begin
  Result := False;
end;


{****************************************************************************
                              Disk Functions
****************************************************************************}

function DiskFree(drive: Byte): Int64;
begin
  Result := 0;
end;

function DiskSize(drive: Byte): Int64;
begin
  Result := 0;
end;

function GetCurrentDir: String;
begin
  GetDir(0, Result);
end;

function SetCurrentDir(const NewDir: String): Boolean;
var
  Path: String;
begin
  ChDir(NewDir);
  GetDir(0, Path);
  Result := LowerCase(ExcludeTrailingPathDelimiter(Path)) = LowerCase(ExcludeTrailingPathDelimiter(NewDir));
end;

function CreateDir(const NewDir: String): Boolean;
begin
  Result := False;
end;

function RemoveDir(const Dir: String): Boolean;
begin
  Result := False;
end;


{****************************************************************************
                              Time Functions
****************************************************************************}

procedure GetLocalTime(var SystemTime: TSystemTime);
var
  kos_time: TKosTime;
  kos_date: TKosDate;
begin
  kos_time := kos_gettime();
  kos_date := kos_getdate();
  SystemTime.Year   := kos_date.Year;
  SystemTime.Month  := kos_date.Month;
  SystemTime.Day    := kos_date.Day;
  SystemTime.Hour   := kos_time.Hour;
  SystemTime.Minute := kos_time.Minute;
  SystemTime.Second := kos_time.Second;
end;


{****************************************************************************
                              Misc Functions
****************************************************************************}

procedure Beep;
begin
end;


{****************************************************************************
                              Locale Functions
****************************************************************************}

procedure GetFormatSettings;
var
  HF: String;
begin
  ShortMonthNames[1] := SShortMonthNameJan;
  ShortMonthNames[2] := SShortMonthNameFeb;
  ShortMonthNames[3] := SShortMonthNameMar;
  ShortMonthNames[4] := SShortMonthNameApr;
  ShortMonthNames[5] := SShortMonthNameMay;
  ShortMonthNames[6] := SShortMonthNameJun;
  ShortMonthNames[7] := SShortMonthNameJul;
  ShortMonthNames[8] := SShortMonthNameAug;
  ShortMonthNames[9] := SShortMonthNameSep;
  ShortMonthNames[10] := SShortMonthNameOct;
  ShortMonthNames[11] := SShortMonthNameNov;
  ShortMonthNames[12] := SShortMonthNameDec;

  LongMonthNames[1] := SLongMonthNameJan;
  LongMonthNames[2] := SLongMonthNameFeb;
  LongMonthNames[3] := SLongMonthNameMar;
  LongMonthNames[4] := SLongMonthNameApr;
  LongMonthNames[5] := SLongMonthNameMay;
  LongMonthNames[6] := SLongMonthNameJun;
  LongMonthNames[7] := SLongMonthNameJul;
  LongMonthNames[8] := SLongMonthNameAug;
  LongMonthNames[9] := SLongMonthNameSep;
  LongMonthNames[10] := SLongMonthNameOct;
  LongMonthNames[11] := SLongMonthNameNov;
  LongMonthNames[12] := SLongMonthNameDec;

  ShortDayNames[1] := SShortDayNameMon;
  ShortDayNames[2] := SShortDayNameTue;
  ShortDayNames[3] := SShortDayNameWed;
  ShortDayNames[4] := SShortDayNameThu;
  ShortDayNames[5] := SShortDayNameFri;
  ShortDayNames[6] := SShortDayNameSat;
  ShortDayNames[7] := SShortDayNameSun;

  LongDayNames[1] := SLongDayNameMon;
  LongDayNames[2] := SLongDayNameTue;
  LongDayNames[3] := SLongDayNameWed;
  LongDayNames[4] := SLongDayNameThu;
  LongDayNames[5] := SLongDayNameFri;
  LongDayNames[6] := SLongDayNameSat;
  LongDayNames[7] := SShortDayNameSun;

  DateSeparator := '/';
  ShortDateFormat := 'd/mm/yy';
  LongDateFormat := 'd mmmm yyyy';
  { Time stuff }
  TimeSeparator := ':';
  TimeAMString := 'AM';
  TimePMString := 'PM';
  HF := 'hh';
  // No support for 12 hour stuff at the moment...
  ShortTimeFormat := HF + ':nn';
  LongTimeFormat := HF + ':nn:ss';
  { Currency stuff }
  CurrencyString := '';
  CurrencyFormat := 0;
  NegCurrFormat := 0;
  { Number stuff }
  ThousandSeparator := ',';
  DecimalSeparator := '.';
  CurrencyDecimals := 2;
end;

procedure InitInternational;
begin
  InitInternationalGeneric;
  GetFormatSettings;
end;

function SysErrorMessage(ErrorCode: Integer): String;
const
  MaxMsgSize = 255;
var
  MsgBuffer: PChar;
begin
  GetMem(MsgBuffer, MaxMsgSize);
  FillChar(MsgBuffer^, MaxMsgSize, #0);
  {TODO}
  Result := StrPas(MsgBuffer);
  FreeMem(MsgBuffer, MaxMsgSize);
end;

function GetEnvironmentVariable(const EnvVar: String): String;
var
  I   : Integer;
  Name: String;
begin
  Name := UpCase(EnvVar);
  for I := Low(KosEnvironment) to High(KosEnvironment) do
  if KosEnvironment[I].Name = Name then
  begin
    Result := KosEnvironment[I].Value;
    Exit;
  end;
  Result := '';
end;

function GetEnvironmentVariableCount: Integer;
begin
  Result := Length(KosEnvironment);
end;

function GetEnvironmentString(Index: Integer): String;
begin
  Dec(Index);
  if 0 < Length(KosEnvironment) then
  begin
    if Index < Low(KosEnvironment) then Index := Low(KosEnvironment) else
    if High(KosEnvironment) < Index then Index := High(KosEnvironment);
    Result := KosEnvironment[Index].Name + '=' + KosEnvironment[Index].Value;
  end else
    Result := '';
end;

function ExecuteProcess(const Path: AnsiString; const ComLine: AnsiString; Flags: TExecuteFlags=[]): Integer;

  function MakeError(ErrNo: Longint): EOSError;
  begin
    Result := EOSError.CreateFmt(SExecuteProcessFailed, [Path, ErrNo]);
    Result.ErrorCode := ErrNo;
  end;

var
  F  : File;
  KF : PKosFile;
  Pid: TThreadID;
  FullPath: AnsiString;
  KoTid  : TThreadID;
  XpcData: AnsiString;

begin
  { @xxx: ну тут assign/reset/close и не нужны вовсе, могут даже мешать, но мне
          лень повторять всю ту работу (формирование поля `Name`) что делается в
          `do_open` }

  if Length(Path) = 0 then
    raise MakeError(1);

  if (0 < Length(Path)) and (Path[1] <> '/') and (Pos('/', Path) = 0) then
  begin
    FullPath := IncludeTrailingPathDelimiter(GetCurrentDir) + Path;
    if not FileExists(FullPath) then
      FullPath := ExeSearch(Path, GetEnvironmentVariable('PATH'));
  end else
    FullPath := Path;

  KoTid := KonsoleGetThreadID;

  XpcData := '$parent.pid=' + IntToStr(GetProcessID) +
            ',$parent.cwd=' + GetCurrentDir;

  if 0 < KoTid then
    XpcData := ',$terminal.pid=' + IntToStr(KoTid);

  Assign(F, FullPath);
  Reset(F);

  if IOResult <> 0 then
    raise MakeError(IOResult);

  KF := PKosFile(FileRec(F).Handle);
  KF^.Position := QWord(@ComLine[1]) << 32;
  if XpcData <> '' then
    KF^.Position := KF^.Position or 1;  { bit0 -- debug mode }
  KF^.Size := 0;
  KF^.Data := nil;

  Pid := kos_startapp(KF);

  Close(F);

  if Pid < 0 then
    raise MakeError(-Pid);  { @todo: error code }

  if XpcData <> '' then
    if not InjectKosXPC(Pid, Length(XpcData) + 1, @XpcData[1]) then
      raise MakeError(-2);

  Result := WaitForThreadTerminate(Pid, 0);
end;

function ExecuteProcess(const Path: AnsiString; const ComLine: array of AnsiString; Flags: TExecuteFlags=[]): Integer;
var
  I         : Integer;
  StrComLine: AnsiString;
begin
  StrComLine := '';
  for I := Low(ComLine) to High(ComLine) do
  if ComLine[I] <> '' then
  begin
    if StrComLine <> '' then
      StrComLine := StrComLine + ' ';
    StrComLine := StrComLine + ComLine[I];
  end;
  Result := ExecuteProcess(Path, StrComLine, Flags);
end;

procedure Sleep(Milliseconds: Cardinal);
begin
  kos_delay(Milliseconds div 10);
end;

function GetLastOSError: Integer;
begin
  Result := -1;
end;


initialization
  InitExceptions;
  InitInternational;
finalization
  DoneExceptions;
end.
