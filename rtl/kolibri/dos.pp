{$codepage utf8}

unit Dos;

interface

{$define HAS_FILENAMELEN}
const
  FileNameLen = 255;

type
  SearchRec = record
    Name: String[FileNameLen];
  end;

{$i dosh.inc}

implementation

{-$DEFINE FPC_FEXPAND_TILDE}       { Tilde is expanded to home }
{-$DEFINE FPC_FEXPAND_GETENVPCHAR} { GetEnv result is a PChar }
{-$DEFINE FPC_FEXPAND_UNC}         { UNC paths are supported }
{-$DEFINE FPC_FEXPAND_DRIVES}      { Full paths begin with drive specification }

{$i dos.inc}

procedure NotImplementedError(What: String);
begin
  WriteLn(StdErr, '"' + What + '" not implemented.');
  RunError(1);
end;

function DosVersion: Word;
begin
  NotImplementedError('DosVersion');
  DosVersion := 0;
end;

{ ==== Info / Date / Time ==== }
procedure GetDate(var Year, Month, MDay, WDay: Word);
begin
  NotImplementedError('GetDate');
end;

procedure GetTime(var Hour, Minute, Second, Sec100: Word);
begin
  NotImplementedError('GetTime');
end;

procedure SetDate(Year, Month, Day: Word);
begin
  NotImplementedError('SetDate');
end;

procedure SetTime(Hour, Minute, Second, Sec100: Word);
begin
  NotImplementedError('SetTime');
end;

{ ==== Exec ==== }
procedure Exec(const Path: PathStr; const ComLine: ComStr);
begin
  NotImplementedError('Exec');
  LastDosExitCode := 0;
  DosError := 0;
end;

{ ==== Disk ==== }
function DiskFree(Drive: Byte): Int64;
begin
  NotImplementedError('DiskFree');
  DiskFree := 0;
end;

function DiskSize(Drive: Byte): Int64;
begin
  NotImplementedError('DiskSize');
  DiskSize := 0;
end;

procedure FindFirst(const Path: PathStr; Attr: Word; var F: SearchRec);
begin
  NotImplementedError('FindFirst');
end;

procedure FindNext(var F: SearchRec);
begin
  NotImplementedError('FindNext');
end;

procedure FindClose(var F: SearchRec);
begin
  NotImplementedError('FindClose');
end;

{ ==== File ==== }
procedure GetFAttr(var F; var Attr: Word);
begin
  NotImplementedError('GetFAttr');
end;

procedure GetFTime(var F; var Time: Longint);
begin
  NotImplementedError('GetFTime');
end;

procedure SetFAttr(var F; Attr: Word);
begin
  NotImplementedError('SetFAttr');
end;

procedure SetFTime(var F; Time: Longint);
begin
  NotImplementedError('SetFTime');
end;

function FSearch(Path: PathStr; DirList: String): PathStr;
begin
  NotImplementedError('FSearch');
  FSearch := '';
end;

{ ==== Environment ==== }

function EnvCount: Longint;
begin
  EnvCount := Length(KosEnvironment);
end;

function EnvStr(Index: Longint): String;
begin
  Dec(Index);
  if 0 < Length(KosEnvironment) then
  begin
    if Index < Low(KosEnvironment) then Index := Low(KosEnvironment) else
    if High(KosEnvironment) < Index then Index := High(KosEnvironment);
    EnvStr := KosEnvironment[Index].Name + '=' + KosEnvironment[Index].Value;
  end else
    EnvStr := '';
end;

function GetEnv(EnvVar: String): String;
var
  I   : Integer;
  Name: String;
begin
  Name := UpCase(EnvVar);
  for I := Low(KosEnvironment) to High(KosEnvironment) do
  if KosEnvironment[I].Name = Name then
  begin
    GetEnv := KosEnvironment[I].Value;
    Exit;
  end;
  GetEnv := '';
end;


end.
