{-$codepage utf8}

procedure WriteLnKosBoard(const S: String);
function InjectKosXPC(Pid: DWord; Size: DWord; Data: Pointer): Boolean;

type
  TKosPoint = packed record
    X: Longint;
    Y: Longint;
  end;

  TKosRect = packed record
  case Integer of
    0: (Left, Top, Width, Height: Longint);
    1: (TopLeft, HeightWidth: TKosPoint);
  end;

{ User interface }

procedure kos_definewindow(x, y, w, h: Word; style: DWord = $23FFFFFF; header: DWord = $008899FF; clframe: DWord = $008899FF);
procedure kos_movewindow(x, y, w, h: DWord);
function kos_getkey(): Word;
function kos_getevent(wait: Boolean = True): DWord;
function kos_waitevent(timeout: DWord): DWord;
function kos_getbutton(): DWord;
function kos_getmousepos(): TKosPoint;
function kos_getmousewinpos(): TKosPoint;
function kos_getmousebuttons(): DWord;
procedure kos_maskevents(mask: DWord);
procedure kos_setkeyboardmode(mode: DWord);
function  kos_getkeyboardmode(): DWord;
function  kos_getcontrolkeys(): Word;
procedure kos_setcaption(caption: PChar);

{ Graphics }

function kos_screensize(): TKosPoint;
procedure kos_begindraw();
procedure kos_enddraw();
procedure kos_putpixel(x, y: DWord; color: DWord = $000000);
procedure kos_drawtext(x, y: DWord; text: String; flags: DWord = $000000; bgcolor: DWord = $00FFFFFF);
procedure kos_drawrect(x, y, w, h: DWord; color: DWord = $000000);
procedure kos_drawline(x1, y1, x2, y2: DWord; color: DWord = $000000);
procedure kos_drawimage(x, y, w, h, depth: DWord; image: Pointer; palette: Pointer = nil; xoffset: DWord = 0);
procedure kos_drawimage24(x, y, w, h: DWord; image: Pointer);

{ Work with system }

type
  TKosTime = packed record
    Hour, Minute, Second, Unused: Byte;
  end;

  TKosDate = packed record
    Year : Word;
    Month: Byte;
    Day  : Byte;
  end;

function kos_gettime(): TKosTime;
function kos_getdate(): TKosDate;

{ Work with system - System services }

function kos_killthread(tid: TThreadID): Boolean;
procedure kos_setactivewindow(slot: TThreadSlot);
function kos_getthreadslot(tid: TThreadID): TThreadSlot;

{ Work with system - Set system parameters }

procedure kos_enablepci();

{ Work with system - Get system parameters }

function kos_timecounter(): DWord;

procedure kos_writedebug(c: Char);
function kos_readdebug(): Integer;

{ Work with system - Internal system services }

procedure kos_switchthread();
function kos_initheap(): DWord;
function kos_alloc(size: DWord): Pointer;
function kos_free(ptr: Pointer): Boolean;

type
  PKosDriver = ^TKosDriver;
  TKosDriver = packed record
    Handle    : THandle;
    Func      : DWord;
    Data      : Pointer;
    DataSize  : DWord;
    Result    : Pointer;
    ResultSize: DWord;
  end;

function kos_loaddriver(name: PChar): THandle;
function kos_controldriver(control: PKosDriver): Longint;

{ Processes and threads }

const
  STILL_ACTIVE = $103;

type
  PKosThreadInfo = ^TKosThreadInfo;
  TKosThreadInfo = packed record
    Speed      : DWord;
    WindowID   : Word;
    ThreadSlot : Word;
    Reserved1  : Word;
    AppName    : array[0..10] of Char;
    Reserved2  : Byte;
    ProcessBase: Pointer;
    MemoryUsage: DWord;
    ThreadID   : TThreadID;
    WindowRect : TKosRect;
    Status     : Word;
    Reserved3  : Word;
    ClientRect : TKosRect;
    Reserved4  : array[1..1046] of Byte;
  end;

  {Буфер IPC}
  PKosIPC = ^TKosIPC;
  TKosIPC = packed record
    Lock: LongBool;
    Size: DWord;
    {сообщение #1...}
    {сообщение #2...}
    {...}
  end;

  {Сообщение IPC}
  PKosMessage = ^TKosMessage;
  TKosMessage = packed record
    SenderID: TThreadID;
    Size    : DWord;
    {тело сообщения...}
  end;

const
  KOS_THREAD_RUNNING   = 0;
  KOS_THREAD_SUSPENDED = 1;
  KOS_THREAD_SUSPENDED_EVENT = 2;
  KOS_THREAD_TERMINATING_OK  = 3;
  KOS_THREAD_TERMINATING_EXC = 4;
  KOS_THREAD_WAITS_EVENT     = 5;
  KOS_THREAD_FREE_SLOT       = 9;

function kos_threadinfo(info: PKosThreadInfo; slot: TThreadSlot = -1): DWord;
function kos_newthread(entry, stack: Pointer): TThreadID;
procedure kos_initipc(ipc: PKosIPC; size: DWord);
function kos_sendmsg(tid: TThreadID; msg: Pointer; size: DWord): DWord;
function kos_resizemem(size: DWord): Boolean;

{function kos_createfutex(pvalue: PDWord): THandle;
 function kos_destroyfutex(handle: THandle): Longint;
 function kos_waitfutex(handle: THandle; value: DWord; timeout: DWord = 0): Longint;
 function kos_wakefutex(handle: THandle; count: DWord): DWord;}

{ File system }
{ File system - Work with the current folder }

procedure kos_setdir(path: PChar);
function kos_getdir(path: PChar; size: DWord): DWord;

{ File system - Work with file system with long names support }

const
  kfReadOnly   = $01;
  kfHidden     = $02;
  kfSystem     = $04;
  kfLabel      = $08;
  kfFolder     = $10;
  kfNotArchive = $20;

type
  PKosFile = ^TKosFile;
  TKosFile = packed record
    SubFunc: DWord;
    Position: QWord;  {flags(4)+params(4) для `kos_startapp`}
    Size: DWord;
    Data: Pointer;                   {BDFE}
    Name: array[0..0] of Char;  {...ASCIIZ}
  end;

  PKosAsciiBDFE = ^TKosAsciiBDFE;
  TKosAsciiBDFE = packed record
    Attributes: DWord;  {bit0:ro, bit1:hidden, bit2:system, bit3:volume, bit4:dir, bit5:arhive}
    NameType: Byte;     {0:ascii, 1:unicode}
    Reserved: array[0..2] of Byte;
    CTime: DWord;  {ss,mm,hh,00}
    CDate: DWord;  {dd,mm,yyyy}
    ATime: DWord;
    ADate: DWord;
    MTime: DWord;
    MDate: DWord;
    Size: QWord;
    Name: array[0..263] of Char;
  end;

  TKosBDFE = TKosAsciiBDFE;

  PKosReadDir = ^TKosReadDir;
  TKosReadDir = packed record
    Version   : DWord;  {=1}
    ReadCount : DWord;
    FilesTotal: DWord;
    Reserved  : array[1..20] of Byte;
    {BDFE #1...}
    {BDFE #2...}
    {...}
  end;

  PKosThreadContent = ^TKosThreadContent;
  TKosThreadContent = packed record
    eip   : DWord;
    eflags: DWord;
    eax, ecx, edx, ebx: DWord;
    esp, ebp, esi, edi: DWord;
  end;

function kos_readdir(kosfile: PKosFile; var readed: Longint): DWord;
function kos_readfile(kosfile: PKosFile; var readed: Longint): DWord;
function kos_rewritefile(kosfile: PKosFile; var writed: Longint): DWord;
function kos_writefile(kosfile: PKosFile; var writed: Longint): DWord;
function kos_fileinfo(kosfile: PKosFile): DWord;
function kos_startapp(kosfile: PKosFile): Longint;

{ Debugging }

procedure kos_debuggetregisters(pid: DWord; content: PKosThreadContent);
procedure kos_debugdetach(pid: DWord);
function  kos_debugwritememory(pid: DWord; size: DWord; data: Pointer; address: Pointer): Longint;
procedure kos_debugkill(pid: DWord);

{ Sound }

function kos_speaker(notes: Pointer): Boolean;

{ Work with hardware }

function kos_readport(index: DWord): DWord;
procedure kos_writeport(index, value: DWord);
function kos_reserveport(port: DWord): Boolean;

{ Work with hardware - Low-level access to PCI}

function kos_lastpcibus(): Byte;
function kos_readpcib(bus, dev, func, reg: Byte): Byte;
function kos_readpciw(bus, dev, func, reg: Byte): Word;
function kos_readpcid(bus, dev, func, reg: Byte): DWord;

{ Other }

procedure kos_delay(ms: DWord);  {1/100 s}

{ Work with network }

{ [bw] }
type
  TKosSign = array[0..7] of Byte;
  PKosHeader = ^TKosHeader;
  TKosHeader = packed record
    sign   : TKOSSign;
    version: DWord;
    start  : DWord;
    size   : DWord;
    memory : DWord;
    stack  : DWord;
    args   : PChar;
    path   : PChar;
  end;
