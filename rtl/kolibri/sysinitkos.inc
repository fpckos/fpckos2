
var
  InitialStkPtr: Pointer; external name '__stkptr';
  XpcDataPtr   : Pointer; external name '__xpcptr';

procedure PascalMain; stdcall; external name 'PASCALMAIN';
procedure SystemExit; external name 'SystemExit';

procedure StartProgram; assembler; nostackframe;
label
  done;
asm
  xorl %eax, %eax
  movl %eax, XpcDataPtr

  subl $8, %esp
  popl %eax
  popl %ebx

  cmpl $0x30435058, %eax  { 'XPC0' }
  jne done
  cmpw $0x0030, %bx       { '0'#0  }
  jne done

  subl   $2, %esp
  xorl %eax, %eax
  popw %ax
  subl $8, %esp
  shll $2, %eax
  subl %eax, %esp
  movl %esp, XpcDataPtr

done:
  pushl $0
  pushl $0
  movl %esp, InitialStkPtr

  call PascalMain
  call SystemExit

  movl $-1, %eax
  int  $0x40
end;
