
type
  PResourceNode = ^TResourceNode;
  TResourceNode = record
    IntId   : Longint;
    Name    : AnsiString;
    Codepage: Longword;
    Size    : Longword;
    Data    : Pointer;
    Children: array of PResourceNode;
  end;

var
  RootTable    : Pointer; external name 'FPC_WINRESTABLE';
  ResourcesRoot: TResourceNode;


procedure KosInitResources;
type
  PResDirTable = ^TResDirTable;
  TResDirTable = packed record
    Characteristics  : Longword;
    TimeStamp        : Longword;
    VerMajor         : Word;
    VerMinor         : Word;
    NamedEntriesCount: Word;
    IDEntriesCount   : Word;
  end;

  PResDirEntry = ^TResDirEntry;
  TResDirEntry = packed record
    NameID       : Longint;
    DataSubDirRVA: Longword;
  end;

  PResDataEntry = ^TResDataEntry;
  TResDataEntry = packed record
    DataRVA : Longword;
    Size    : Longword;
    Codepage: Longword;
    Reserved: Longword;
  end;

  procedure ReadNodeDirEntry(var Node: TResourceNode; Entry: PResDirEntry); forward;
  procedure ReadResDataEntry(var Node: TResourceNode; Entry: PResDataEntry); forward;

  function ReadResString(aRVA: Longword): String;
  var
    WS: WideString;
    WL: Word;
  begin
    WL := PWord(RootTable + aRVA)^;
    SetString(WS, PWideChar(RootTable + aRVA + 2), WL);
    Result := WS;
  end;

  procedure ReadNodeTable(var Node: TResourceNode; Table: PResDirTable);
  var
    I    : Integer;
    P    : PResDirEntry;
    Child: PResourceNode;
  begin
    P := Pointer(Table) + SizeOf(Table^);

    SetLength(Node.Children, Table^.NamedEntriesCount + Table^.IDEntriesCount);

    for I := Low(Node.Children) to High(Node.Children) do
    begin
      New(Child);
      Node.Children[I] := Child;
      ReadNodeDirEntry(Child^, P);
      Inc(P);
    end;
  end;

  procedure ReadNodeDirEntry(var Node: TResourceNode; Entry: PResDirEntry);
  var
    Name : String;
    Ident: Longword;
  begin
    if (Entry^.NameID and $80000000) <> 0 then
    begin
      Node.Name  := ReadResString(Entry^.NameID and $7FFFFFFF);
      Node.IntId := Longint(@Node.Name[1]);

      if Is_IntResource(PChar(Node.IntId)) then
      begin
        WriteLn(StdErr, 'Can''t allocate resource name (', Node.Name, ')');
        RunError(410);
      end;
    end else
      Node.IntId := Entry^.NameID;

    if (Entry^.DataSubDirRVA and $80000000) <> 0 then
      ReadNodeTable(Node, PResDirTable(RootTable + (Entry^.DataSubDirRVA and $7FFFFFFF))) else
      ReadResDataEntry(Node, PResDataEntry(RootTable + Entry^.DataSubDirRVA));
  end;

  procedure ReadResDataEntry(var Node: TResourceNode; Entry: PResDataEntry);
  begin
    Node.Codepage := Entry^.Codepage;
    Node.Size     := Entry^.Size;
    Node.Data     := Pointer(Entry^.DataRVA);
  end;

begin
  ReadNodeTable(ResourcesRoot, PResDirTable(RootTable));
end;


function StrIComp(Str1, Str2: PChar): SizeInt;
var
  Counter: SizeInt;
  C1, C2 : Char;
begin
  Counter := 0;

  C1 := UpCase(Str1[Counter]);
  C2 := UpCase(Str2[Counter]);

  while C1 = C2 do
  begin
    if (C1 = #0) or (C2 = #0) then Break;
    Inc(Counter);
    C1 := UpCase(Str1[Counter]);
    C2 := UpCase(Str2[Counter]);
  end;

  Result := Ord(C1) - Ord(C2);
end;


function KosHINSTANCE: TFPResourceHMODULE;
begin
  Result := 0;
end;

function KosEnumResourceTypes(ModuleHandle: TFPResourceHMODULE; EnumFunc: EnumResTypeProc; lParam: PtrInt): LongBool;
var
  I: Integer;
begin
  for I := Low(ResourcesRoot.Children) to High(ResourcesRoot.Children) do
    { @xxx: не проверяю повторы }
    EnumFunc(ModuleHandle, PChar(ResourcesRoot.Children[I]^.IntId), lParam);
  Result := True;
end;

function KosEnumResourceNames(ModuleHandle: TFPResourceHMODULE; ResourceType: PChar; EnumFunc: EnumResNameProc; lParam: PtrInt): LongBool;
var
  I, J: Integer;
  IsInt: Boolean;
begin
  IsInt := Is_IntResource(ResourceType);

  for I := Low(ResourcesRoot.Children) to High(ResourcesRoot.Children) do
  with ResourcesRoot.Children[I]^ do
  if (IsInt and (0 <= IntId) and (ResourceType = PChar(IntId))) or (not IsInt and (IntId < 0) and (StrIComp(ResourceType, PChar(IntId)) = 0)) then
    for J := Low(Children) to High(Children) do
      EnumFunc(ModuleHandle, ResourceType, PChar(Children[J]^.IntId), lParam);

  Result := True;
end;

function KosEnumResourceLanguages(ModuleHandle: TFPResourceHMODULE; ResourceType, ResourceName: PChar; EnumFunc: EnumResLangProc; lParam: PtrInt): LongBool;
begin
  Result := False;
end;

function KosFindResource(ModuleHandle: TFPResourceHMODULE; ResourceName, ResourceType: PChar): TFPResourceHandle;

  function IsEqual(ValueIsInt: Boolean; Value: PChar; Node: TResourceNode): Boolean;
  begin
    if ValueIsInt and Is_IntResource(PChar(Node.IntId)) and (Value = PChar(Node.IntId)) then
      Result := True else
    if not ValueIsInt and not Is_IntResource(PChar(Node.IntId)) and (StrIComp(Value, PChar(Node.IntId)) = 0) then
      Result := True else
      Result := False;
  end;

var
  I, J     : Integer;
  TypeIsInt: Boolean;
  NameIsInt: Boolean;
begin
  TypeIsInt := Is_IntResource(ResourceType);
  NameIsInt := Is_IntResource(ResourceName);

  for I := Low(ResourcesRoot.Children) to High(ResourcesRoot.Children) do
  with ResourcesRoot do
  if IsEqual(TypeIsInt, ResourceType, Children[I]^) then
    with Children[I]^ do
    for J := Low(Children) to High(Children) do
    if IsEqual(NameIsInt, ResourceName, Children[J]^) then
      with Children[J]^ do
      if 1 <= Length(Children) then  { @note: я не встречал других вариантов, это должны быть языки }
      begin
        Result := TFPResourceHandle(Children[Low(Children)]);
        Exit;
      end;

  Result := 0;
end;

function KosFindResourceEx(ModuleHandle: TFPResourceHMODULE; ResourceType, ResourceName: PChar; Language: Word): TFPResourceHandle;
begin
  Result := KosFindResource(ModuleHandle, ResourceName, ResourceType);
end;

function KosLoadResource(ModuleHandle: TFPResourceHMODULE; ResHandle: TFPResourceHandle): TFPResourceHGLOBAL;
begin
  Result := ResHandle;
end;

function KosSizeOfResource(ModuleHandle: TFPResourceHMODULE; ResHandle: TFPResourceHandle): Longword;
begin
  Result := PResourceNode(ResHandle)^.Size;
end;

function KosLockResource(ResData: TFPResourceHGLOBAL): Pointer;
begin
  Result := PResourceNode(ResData)^.Data;
end;

function KosUnlockResource(ResData: TFPResourceHGLOBAL): LongBool;
begin
  Result := True;
end;

function KosFreeResource(ResData: TFPResourceHGLOBAL): LongBool;
begin
  { @todo: ? }
  Result := True;
end;


const
  InternalResourceManager: TResourceManager = (
    HINSTANCEFunc     : @KosHINSTANCE;
    EnumResourceTypesFunc    : @KosEnumResourceTypes;
    EnumResourceNamesFunc    : @KosEnumResourceNames;
    EnumResourceLanguagesFunc: @KosEnumResourceLanguages;
    FindResourceFunc  : @KosFindResource;
    FindResourceExFunc: @KosFindResourceEx;
    LoadResourceFunc  : @KosLoadResource;
    SizeofResourceFunc: @KosSizeofResource;
    LockResourceFunc  : @KosLockResource;
    UnlockResourceFunc: @KosUnlockResource;
    FreeResourceFunc  : @KosFreeResource;
  );
