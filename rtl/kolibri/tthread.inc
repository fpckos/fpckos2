{-misplaced-$codepage utf8}

{-$MACRO ON}

const
  Priorities: array[TThreadPriority] of Integer = (0, 0, 0, 0, 0, 0, 0);
  KosDefaultStackSize = 256*1024;  // inc/threadh.inc --> DefaultStackSize = 4Mb


constructor TThread.Create(CreateSuspended: Boolean; const StackSize: SizeUInt=DefaultStackSize);
var
  Flags: Integer;
  SSize: SizeUInt;
begin
  inherited Create;

  if StackSize = DefaultStackSize then
    { @xxx: не могу переопределить `DefaultStackSize`, так что так }
    SSize := KosDefaultStackSize else
    SSize := StackSize;

  if CreateSuspended then
    { @xxx: вообще-то можно схитрить, но мне лень...
            хитрить, думаю, нужно в `systhrd.inc` }
    raise EThread.Create('Can''t create suspended, suspending don''t supported');

  Flags := 0;
  {if CreateSuspended then Flags := CREATE_SUSPENDED;}
  FHandle := BeginThread(nil, SSize, @ThreadProc, Pointer(Self), Flags, FThreadID);

  if FHandle = 0 then
    raise EThread.Create('Failed to create new thread (there is too many threads)');

  FSuspended := CreateSuspended;
  FFatalException := nil;
end;

destructor TThread.Destroy;
begin
  if FHandle <> 0 then
  begin
    if not FFinished and not Suspended then
    begin
      Terminate;
      WaitFor;
    end;
    FHandle := 0;
  end;

  FFatalException.Free;
  FFatalException := nil;

  inherited Destroy;
end;

procedure TThread.CallOnTerminate;
begin
  FOnTerminate(self);
end;

procedure TThread.DoTerminate;
begin
  if Assigned(FOnTerminate) then
    Synchronize(@CallOnTerminate);
end;

function TThread.GetPriority: TThreadPriority;
var
  P: Integer;
  I: TThreadPriority;
begin
  P := ThreadGetPriority(FHandle);
  Result := tpNormal;
  for I := Low(TThreadPriority) to High(TThreadPriority) do
    if Priorities[I] = P then Result := I;
end;

procedure TThread.SetPriority(Value: TThreadPriority);
begin
  ThreadSetPriority(FHandle, Priorities[Value]);
end;

procedure TThread.SetSuspended(Value: Boolean);
begin
  if Value <> FSuspended then
  if Value then Suspend else Resume; // Symbols "Suspend" & "Resume" is deprecated
end;

procedure TThread.Suspend;
begin
  if SuspendThread(FHandle) = 0 then
    FSuspended := True;
end;

procedure TThread.Resume;
begin
  if ResumeThread(FHandle) = 0 then
    FSuspended := False;
end;

procedure TThread.Terminate;
begin
  if KillThread(FHandle) = 0 then
    FTerminated := True;
end;

function TThread.WaitFor: Integer;
begin
  Result := WaitForThreadTerminate(FHandle, 0);
end;
