{-$codepage utf8}

procedure OpenStdout(var f: TextRec); forward;
procedure WriteStdout(var f: TextRec); forward;
procedure FlushStdout(var f: TextRec); forward;
procedure CloseStdout(var f: TextRec); forward;

procedure OpenStdin(var f: TextRec); forward;
procedure ReadStdin(var f: TextRec); forward;
procedure CloseStdin(var f: TextRec); forward;


procedure WriteLnKosBoard(const S: String);
var
  C: PChar;
begin
  if 0 < Length(S) then
  begin
    C := @S[1];
    while C^ <> #0 do
    begin
      kos_writedebug(C^);
      Inc(C);
    end;
    kos_writedebug(#10);
  end;
end;

procedure AssignStdout(var f: Text);
begin
  Assign(f, '');
  TextRec(f).OpenFunc := @OpenStdout;
  Rewrite(f);
end;

procedure OpenStdout(var f: TextRec);
begin
  TextRec(f).InOutFunc := @WriteStdout;
  TextRec(f).FlushFunc := @FlushStdout;
  TextRec(f).CloseFunc := @CloseStdout;
end;

procedure WriteStdout(var f: TextRec);
var
  C: PChar;
begin
  C := PChar(f.bufptr);
  while f.bufpos > 0 do
  begin
    kos_writedebug(C^);
    Inc(C);
    Dec(f.bufpos);
  end;
end;

procedure FlushStdout(var f: TextRec);
begin
  WriteStdout(f);
end;

procedure CloseStdout(var f: TextRec);
begin
end;


procedure AssignStdin(var f: Text);
begin
  Assign(f, '');
  TextRec(f).OpenFunc := @OpenStdin;
  Reset(f);
end;

procedure OpenStdin(var f: TextRec);
begin
  TextRec(f).InOutFunc := @ReadStdin;
  TextRec(f).FlushFunc := nil;
  TextRec(f).CloseFunc := @CloseStdin;
end;

procedure ReadStdin(var f: TextRec);
var
  I, J, Max, Pos: Integer;
  C: PChar;
begin
  Max := f.bufsize - Length(LineEnding);
  Pos := 0;
  C   := PChar(f.bufptr);

  repeat
    if Pos < Max then
      I := kos_readdebug() else
      I := -1;

    if (I < 0) or (I = Ord(LineEnding[1])) then
    begin
      for J := 1 to Length(LineEnding) do
      begin
        C^ := LineEnding[J];
        Inc(C);
        Inc(Pos);
      end;
      f.bufpos := 0;
      f.bufend := Pos;
      Break;
    end;

    for J := 2 to Length(LineEnding) do
    if I = Ord(LineEnding[J]) then
    begin
      I := 256;
      Break
    end;

    if (I < 256) then
    begin
      C^ := Char(I);
      Inc(C);
      Inc(Pos);
    end;
  until False;
end;

procedure CloseStdin(var f: TextRec);
begin
end;
