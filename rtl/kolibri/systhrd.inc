
{$macro on}
{$ifdef DEBUG_MT}
  {$define WriteDebug := WriteLn}
{$else}
  {$define WriteDebug := //}
{$endif}

procedure ThreadError; external name 'FPC_THREADERROR';


procedure NoThreadError(What: String='?'; Halt: Boolean=True);
begin
  WriteLn(StdErr, '"' + What + '" not implemented.');
  if Halt then
    RunError(232);
end;


{XXX: Thread vars & TLS}

const
  ThreadVarBlockSize: DWord = 0;
  TLSGrowFor = 4096;

type
  PTLSIndex = ^TTLSIndex;
  TTLSIndex = record
    CS: TRTLCriticalSection;
    Slots: array[0..TLSGrowFor - 1] of record
      TID: DWord;
      Value: Pointer;
    end;
  end;

  PPThreadInfo = ^PThreadInfo;
  PThreadInfo = ^TThreadInfo;
  TThreadInfo = record
    { эта структура находится в вершине стека }
    Func     : TThreadFunc;
    Arg      : Pointer;
    StackSize: PtrUInt;
    Stack    : Pointer;
    Flags    : DWord;  { @ex: THREAD_NODAEMON }
    ThreadID : DWord;
    ExitCode : DWord;
  end;

threadvar
  CurrentThreadInfo: PThreadInfo;

var
  TLSKey: PTLSIndex;
  ThreadsCS    : TRTLCriticalSection;
  ActiveThreads: array of TThreadInfo;


function TLSAlloc(): PTLSIndex;
var
  I: DWord;
begin
  {New(Result);}
  Result := kos_alloc(SizeOf(TTLSIndex));
  InitCriticalSection(Result^.CS);
  {SetLength(Result^.Slots, TLSGrowFor);}
  for I := 0 to TLSGrowFor - 1 do
    Result^.Slots[I].TID := 0;
end;


function TLSFree(TLSIndex: PTLSIndex): Boolean;
begin
  DoneCriticalSection(TLSIndex^.CS);
  {SetLength(TLSIndex^.Slots, 0);
  Dispose(TLSIndex);}
  kos_free(TLSIndex);
  Result := True;
end;


procedure TLSSetValue(TLSIndex: PTLSIndex; Value: Pointer);
var
  TID, I, Count, Slot: DWord;
begin
  TID := GetCurrentThreadID();
  EnterCriticalSection(TLSIndex^.CS);

  Count := Length(TLSIndex^.Slots);
  Slot := Count;

  for I := 0 to Count - 1 do
  if TLSIndex^.Slots[I].TID = TID then
  begin
    Slot := I;
    Break;
  end else
  if TLSIndex^.Slots[I].TID = 0 then
    Slot := I;

  if Slot >= Count then
  begin
    Halt(123);
    {SetLength(TLSIndex^.Slots, Count + TLSGrowFor);
    FillChar(TLSIndex^.Slots[Count], TLSGrowFor * SizeOf(TLSIndex^.Slots[0]), #0);
    Slot := Count;}
  end;

  TLSIndex^.Slots[Slot].TID := TID;
  TLSIndex^.Slots[Slot].Value := Value;

  LeaveCriticalSection(TLSIndex^.CS);
end;


function TLSGetValue(TLSIndex: PTLSIndex): Pointer;
var
  TID, I, Count: DWord;
begin
  Result := nil;
  TID := GetCurrentThreadID();

  EnterCriticalSection(TLSIndex^.CS);

  Count := Length(TLSIndex^.Slots);

  for I := 0 to Count - 1 do
  if TLSIndex^.Slots[I].TID = TID then
  begin
    Result := TLSIndex^.Slots[I].Value;
    break;
  end;

  LeaveCriticalSection(TLSIndex^.CS);
end;


procedure FinishThread;
var
  I         : Integer;
  ThreadInfo: PThreadInfo;
  Stack     : Pointer;
begin
  ThreadInfo := CurrentThreadInfo;
  Stack      := ThreadInfo^.Stack;

  EnterCriticalSection(ThreadsCS);
  try
    for I := Low(ActiveThreads) to High(ActiveThreads) do
    if ActiveThreads[I].Stack = Stack then
      ActiveThreads[I].Stack := nil;
  finally
    LeaveCriticalSection(ThreadsCS);
  end;

  DoneThread;  { -> ReleaseThreadVars }
  FreeMem(Stack);

  asm
    movl $-1, %eax
    int $0x40
  end;
end;

procedure SysAllocateThreadVars; forward;

procedure ThreadMain(ThreadInfo: PThreadInfo);

  procedure ShowException;
  var
    EObject: TObject;
    EAddr  : Pointer;
    EFrameCount: Longint;
    EFrames: PPointer;
  begin
    If Assigned(RaiseList) then
    begin
      EObject := RaiseList^.FObject;
      EAddr   := RaiseList^.Addr;
      EFrameCount := RaiseList^.FrameCount;
      EFrames := RaiseList^.Frames;

      { @xxx: мне не понравилась дефолтная `CatchUnhandledException`, там ничего про поток нет
        if Assigned(ExceptProc) then
          TExceptProc(ExceptProc)(EObject, EAddr, EFrameCount, EFrames) else ... }

      WriteLn(StdErr, 'Exception in thread #', ThreadID, ' at $', HexStr(DWord(EAddr), 8), ': ', EObject.ClassName);
      { @note: Не могу тут работать с Exception, так ему нужно SysUtils }
      { @see: `CatchUnhandledException` и `ExceptionErrorMessage` в `SysUtils` }
      DumpExceptionBackTrace(StdErr);
    end else
      WriteLn(StdErr, 'Exception in thread #', ThreadID);
  end;

var
  Result  : PtrInt;
  Stack   : Pointer;
  ThreadID: DWord;
begin
  SysAllocateThreadVars;

  CurrentThreadInfo := ThreadInfo;
  ThreadID := GetCurrentThreadID();
  ThreadInfo^.ThreadID := ThreadID;

  with ThreadInfo^ do
  begin
//  WriteDebug('New thread #', ThreadID, ' started, initializing...');
    InitThread(StackSize);
    try
//     WriteDebug('Jumping to thread #', ThreadID, ' function');
       Result := Func(Arg);  { тут может быть вызван EndThread(ExitCode) }
    except
      ShowException;
    end;
  end;

  WriteDebug('Exit thread #', ThreadID);
  FinishThread;
end;


procedure SysInitThreadVar(var Offset: DWord; Size: DWord);
begin
  Offset := ThreadVarBlockSize;
  Inc(ThreadVarBlockSize, Size);
end;

procedure SysAllocateThreadVars;
var
  DataIndex: Pointer;
begin
//WriteDebug('ThreadVars allocation (', ThreadVarBlockSize, ')...');
  {DataIndex := GetMem(ThreadVarBlockSize);}
  DataIndex := kos_alloc(ThreadVarBlockSize);
  FillChar(DataIndex^, ThreadVarBlockSize, #0);
  TLSSetValue(TLSKey, DataIndex);
//WriteDebug('ThreadVars allocated at $', HexStr(DataIndex));
end;

function SysRelocateThreadVar(Offset: DWord): Pointer;
var
  DataIndex: Pointer;
begin
  DataIndex := TLSGetValue(TLSKey);
  if DataIndex = nil then
  begin
    SysAllocateThreadVars;
    DataIndex := TLSGetValue(TLSKey);
  end;
  Result := DataIndex + Offset;
end;

procedure SysReleaseThreadVars;
{ @see: ThreadMain/EndThread -> DoneThread }
var
  DataIndex: Pointer;
begin
  {FreeMem(TLSGetValue(TLSKey));}
  DataIndex := TLSGetValue(TLSKey);
  kos_free(DataIndex);
//WriteDebug('ThreadVars at $', HexStr(DataIndex), ' released');
end;

function SysBeginThread(sa: Pointer; StackSize: PtrUInt; ThreadFunction: TThreadFunc;
                        Arg: Pointer; CreationFlags: DWord; var ThreadID: TThreadID): TThreadID;

  procedure EntryThreadMain; assembler;
  asm
    movl (%esp), %eax
    jmp ThreadMain
  end;

var
  Stack: Pointer;
  PInfo: PThreadInfo;
  I, J : Integer;
begin
  if not Assigned(TLSKey) then
  begin
    TLSKey := TLSAlloc();
    InitThreadVars(@SysRelocateThreadVar);
  end;

  WriteDebug('Creating new thread (', StackSize, ')...');

  StackSize := (StackSize + 15) and $fffffff0;
  Stack := GetMem(StackSize + SizeOf(PThreadInfo));

  EnterCriticalSection(ThreadsCS);
  try
    J := -1;

    for I := Low(ActiveThreads) to High(ActiveThreads) do
    if not Assigned(ActiveThreads[I].Stack) then
    begin
      { @xxx: если поток был убил, то он останется висеть здесь на всегда (как и везде, где только можно) }
      PInfo := @ActiveThreads[I];
      J := I;
    end;

    if J < 0 then
    begin
      J := Length(ActiveThreads);
      SetLength(ActiveThreads, J + 1);
      PInfo := @ActiveThreads[J];
    end;
  finally
    LeaveCriticalSection(ThreadsCS);
  end;

  PInfo^.Func := ThreadFunction;
  PInfo^.Arg  := Arg;
  PInfo^.StackSize := StackSize;
  PInfo^.Stack := Stack;
  PInfo^.Flags := CreationFlags;
  PInfo^.ThreadID := 0;
  PInfo^.ExitCode := 999999;

  PPThreadInfo(Stack + StackSize)^ := PInfo;

  ThreadID := kos_newthread(@EntryThreadMain, Pointer(Stack + StackSize));
  PInfo^.ThreadID := ThreadID;
  Result := ThreadID;

  WriteDebug('New thread #', ThreadID, ' created successfully');
end;

procedure SysEndThread(ExitCode: DWord);
var
  ThreadID  : DWord;
  ThreadInfo: PThreadInfo;
begin
  { @todo: ExitCode }
  ThreadInfo := CurrentThreadInfo;
  ThreadInfo^.ExitCode := ExitCode;  { @xxx: в этом нет смысла, структура хранится в стеке
                                                    и теряется после его освобождения (см. SysBeginThread) }
  ThreadID := GetCurrentThreadID();

  if ThreadID <> ThreadInfo^.ThreadID then
    { невозможная ситуация }
    WriteLn(StdErr, 'Invalid ThreadID, got ', ThreadID, ' expected ', ThreadInfo^.ThreadID);

  WriteDebug('Done thread #', ThreadID);
  FinishThread;
end;

function NoThreadHandler(ThreadHandle: TThreadID): DWord;
begin
  NoThreadError('NoThreadHandler');
end;

function SysKillThread(ThreadHandle: TThreadID): DWord;
{ @xxx: ведёт к утечке памяти }
begin
  if kos_killthread(ThreadHandle) then
    Result :=  0 else
    Result := -1;
end;

procedure SysThreadSwitch;
begin
{$ifdef EMULATOR}
  kos_delay(0);
{$else}
  kos_switchthread();
{$endif}
end;

function SysWaitForThreadTerminate(ThreadHandle: TThreadID; TimeoutMs: Longint): DWord;  {0=no timeout}
var
  SID: TThreadSlot;
  TI : TKosThreadInfo;
begin
  SID := kos_getthreadslot(ThreadHandle);

  if SID = 0 then
  begin
    Result := 0;  { неверный TID или (?) поток/процесс уже завершился }
    Exit;
  end;

  { @todo: (?) KOS_THREAD_TERMINATING_OK, KOS_THREAD_TERMINATING_EXC }

  if 0 < TimeoutMs then
  begin
    if 1 < TimeoutMs then
      TimeoutMs := TimeoutMs div 10;  // 1/1000 sec -> 1/100 sec
    repeat
      if TimeoutMs < 5 then  { 5 = 1/20 sec }
      begin
        kos_delay(TimeoutMs);
        TimeoutMs := 0;
      end else
      begin
        kos_delay(5);
        Dec(TimeoutMs, 5);  { @xxx: пофиг, пойдёт и так }
      end;
      kos_threadinfo(@TI, SID);
      if TI.ThreadID <> ThreadHandle then
        ThreadError;
    until (TI.Status = KOS_THREAD_FREE_SLOT) or (TimeoutMs <= 0);
  end else
  begin
    repeat
      kos_delay(3);
      kos_threadinfo(@TI, SID);
      if TI.ThreadID <> ThreadHandle then
        ThreadError;
    until TI.Status = KOS_THREAD_FREE_SLOT;
  end;

  if TI.Status = KOS_THREAD_FREE_SLOT then
    Result := 0 else
    Result := 2;
end;

function SysThreadSetPriority(ThreadHandle: TThreadID; Prio: Longint): Boolean; { @ex: -15..+15, 0=normal }
begin
  Result := False;
end;

function SysThreadGetPriority(ThreadHandle: TThreadID): Longint;
begin
  Result := 0;
end;


function SysGetCurrentThreadID: TThreadID;
var
  ThreadInfo: TKosThreadInfo;
begin
  kos_threadinfo(@ThreadInfo);
  Result := ThreadInfo.ThreadID;
end;

{ @xxx: CriticalSection }

procedure SysInitCriticalSection(var CS);
begin
  PRTLCriticalSection(CS)^.OwningThread := -1;
end;

procedure SysDoneCriticalSection(var CS);
begin
  PRTLCriticalSection(CS)^.OwningThread := -1;
end;

procedure SysEnterCriticalSection(var CS);
var
  ThisThread: TThreadID;
begin
  { @todo: mutex }
  ThisThread := GetCurrentThreadId();
  if PRTLCriticalSection(CS)^.OwningThread <> ThisThread then
    while PRTLCriticalSection(CS)^.OwningThread <> -1 do ThreadSwitch;
  PRTLCriticalSection(CS)^.OwningThread := ThisThread;
end;

procedure SysLeaveCriticalSection(var CS);
begin
  if PRTLCriticalSection(CS)^.OwningThread = GetCurrentThreadId() then
    PRTLCriticalSection(CS)^.OwningThread := -1;
end;


function SysBasicEventCreate(EventAttributes: Pointer; AManualReset, InitialState: Boolean; const Name: AnsiString): PEventState;
begin
  NoThreadError('SysBasicEventCreate');
end;

procedure SysBasicEventDestroy(State: PEventState);
begin
  NoThreadError('SysBasicEventDestroy');
end;

procedure SysBasicEventResetEvent(State: PEventState);
begin
  NoThreadError('SysBasicEventResetEvent');
end;

procedure SysBasicEventSetEvent(State: PEventState);
begin
  NoThreadError('SysBasicEventSetEvent');
end;

function SysBasicEventWaitFor(Timeout: Cardinal; State: PEventState): Longint;
begin
  NoThreadError('SysBasicEventWaitFor');
end;

{ @xxx: RTLEvent }
{ @see: Inclocked, Declocked, InterLockedIncrement, InterLockedDecrement,
        InterLockedExchange, InterLockedExchangeAdd, InterlockedCompareExchange }

function SysRTLEventCreate: PRTLEvent;
begin
  New(PKosRTLEvent(Result));
  with PKosRTLEvent(Result)^ do
  begin
    Value   := 0;
    Waiters := 0;
  end;
end;

procedure SysRTLEventDestroy(State: PRTLEvent);
begin
  with PKosRTLEvent(State)^ do
  begin
    Value := -1;
    while 0 < Waiters do ThreadSwitch;
  end;
  FreeMem(PKosRTLEvent(State));
end;

procedure SysRTLEventSetEvent(State: PRTLEvent);
begin
  PKosRTLEvent(State)^.Value := 1;
end;

procedure SysRTLEventResetEvent(State: PRTLEvent);
begin
  PKosRTLEvent(State)^.Value := 0;
end;

procedure SysRTLEventWaitFor(State: PRTLEvent);
begin
  with PKosRTLEvent(State)^ do
  begin
    InterLockedIncrement(Waiters);
    while Value = 0 do ThreadSwitch;
    InterLockedDecrement(Waiters);
  end;
end;

procedure SysRTLEventSync(m: TRTLMethod; p: TProcedure);
begin
  NoThreadError('SysRTLEventSync');
end;

procedure SysRTLEventWaitForTimeout(State: PRTLEvent; Timeout: Longint);
begin
  NoThreadError('SysRTLEventWaitForTimeout');
end;


function SysSemaphoreInit: Pointer;
begin
  NoThreadError('SysSemaphoreInit');
end;

procedure SysSemaphoreWait(const FSem: Pointer);
begin
  NoThreadError('SysSemaphoreWait');
end;

procedure SysSemaphorePost(const FSem: Pointer);
begin
  NoThreadError('SysSemaphorePost');
end;

procedure SysSemaphoreDestroy(const FSem: Pointer);
begin
  NoThreadError('SysSemaphoreDestroy');
end;


procedure InitSystemThreads;
var
  KosThreadManager: TThreadManager;

begin
  ThreadID := TThreadID(-1);
  IsMultiThread := True;

  with KosThreadManager do
  begin
    InitManager            := nil;
    DoneManager            := nil;

    BeginThread            := @SysBeginThread;
    EndThread              := @SysEndThread;
    SuspendThread          := @NoThreadHandler;  {@todo}
    ResumeThread           := @NoThreadHandler;  {@todo}
    KillThread             := @SysKillThread;
    ThreadSwitch           := @SysThreadSwitch;
    WaitForThreadTerminate := @SysWaitForThreadTerminate;
    ThreadSetPriority      := @SysThreadSetPriority;       {@todo}
    ThreadGetPriority      := @SysThreadGetPriority;       {@todo}

    GetCurrentThreadID     := @SysGetCurrentThreadID;
    InitCriticalSection    := @SysInitCriticalSection;
    DoneCriticalSection    := @SysDoneCriticalSection;
    EnterCriticalSection   := @SysEnterCriticalSection;
    LeaveCriticalSection   := @SysLeaveCriticalSection;
    InitThreadVar          := @SysInitThreadVar;
    RelocateThreadVar      := @SysRelocateThreadVar;
    AllocateThreadVars     := @SysAllocateThreadVars;
    ReleaseThreadVars      := @SysReleaseThreadVars;

    BasicEventCreate       := @SysBasicEventCreate;      {@todo}
    BasicEventDestroy      := @SysBasicEventDestroy;     {@todo}
    BasicEventResetEvent   := @SysBasicEventResetEvent;  {@todo}
    BasicEventSetEvent     := @SysBasicEventSetEvent;    {@todo}
    BasicEventWaitFor      := @SysBasicEventWaitFor;     {@todo}

    RTLEventCreate         := @SysRTLEventCreate;          {@todo}
    RTLEventDestroy        := @SysRTLEventDestroy;         {@todo}
    RTLEventSetEvent       := @SysRTLEventSetEvent;        {@todo}
    RTLEventResetEvent     := @SysRTLEventResetEvent;      {@todo}
    RTLEventWaitFor        := @SysRTLEventWaitFor;         {@todo}
    RTLEventSync           := @SysRTLEventSync;            {@todo}
    RTLEventWaitForTimeout := @SysRTLEventWaitForTimeout;  {@todo}

    SemaphoreInit          := @SysSemaphoreInit;
    SemaphoreDestroy       := @SysSemaphoreDestroy;
    SemaphoreWait          := @SysSemaphoreWait;
    SemaphorePost          := @SysSemaphorePost;
  end;
  SetThreadManager(KosThreadManager);

  ThreadID := GetCurrentThreadID;

  InitCriticalSection(ThreadsCS);
end;

procedure DoneSystemThreads;
var
  I    : Integer;
  Waits: array of TThreadID;

  procedure KillThreads(const Force: Boolean=False);
  var
    I: Integer;
  begin
    SetLength(Waits, 0);

    EnterCriticalSection(ThreadsCS);
    try
      for I := Low(ActiveThreads) to High(ActiveThreads) do
      with ActiveThreads[I] do
      if Assigned(Stack) and (ThreadID <> 0) then
      if Force or (Flags and THREAD_NODAEMON = 0) then
      begin
        WriteDebug('[DoneSystemThreads] Kill Thread #', ThreadID);
        KillThread(ThreadID);
        Stack := nil;
      end else
      begin
        SetLength(Waits, Length(Waits) + 1);
        Waits[High(Waits)] := ThreadID;
      end;
    finally
      LeaveCriticalSection(ThreadsCS);
    end;
  end;

begin
  { самоделка, см. изменения в system.inc -> InternalExit }
  WriteDebug('[DoneSystemThreads] ', Length(ActiveThreads));

  KillThreads;

  for I := Low(Waits) to High(Waits) do
  begin
    WriteDebug('[DoneSystemThreads] Wait Thread #', ThreadID);
    WaitForThreadTerminate(Waits[I], 0);
  end;

  KillThreads(True);

  DoneCriticalSection(ThreadsCS);
  { @todo: SetNoThreadManager; }
end;
