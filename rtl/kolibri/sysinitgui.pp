{$codepage utf8}

unit sysinitgui;

interface

implementation

{$i sysinitkos.inc}

procedure StartGUI; assembler; nostackframe; public name '_startGUI';
asm
  movb $0, IsConsole
  jmp StartProgram
end;

function KonsoleGetThreadID: TThreadID; public name 'KonsoleGetThreadID';
begin
  KonsoleGetThreadID := -1;
end;

function KonsoleAssignStdIO: Boolean; public name 'KonsoleAssignStdIO';
begin
  KonsoleAssignStdIO := False;
end;

end.
