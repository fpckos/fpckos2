unit UART;

{$mode objfpc}

interface


procedure ReplaceStdIOWithUart(const Port: Byte=1);
procedure AssignUART(out T: Text; const Port: Byte=1);


implementation


uses
  SysUtils;


{$i textrec.inc}

const
  SRV_GETVERSION = 0;
  PORT_OPEN      = 1;
  PORT_CLOSE     = 2;
  PORT_RESET     = 3;
  PORT_SETMODE   = 4;
  PORT_GETMODE   = 5;
  PORT_SETMCR    = 6;
  PORT_GETMCR    = 7;
  PORT_READ      = 8;
  PORT_WRITE     = 9;

type
  PUartData = ^TUartData;
  TUartData = record
    Port      : Byte;  {1, 2, ...}
    HPort     : THandle;
   {ApiVersion: Longint;}
    Control   : TKosDriver;
  end;

  TUartPortBuffer = packed record
    Port   : THandle;
    Address: Pointer;
    Size   : DWord;
  end;

var
  HDriver: THandle;
  HPorts : array[1..8] of record
    Handle : THandle;
    Counter: Integer;
  end;


procedure UartOpen(var T: TextRec); forward;
procedure UartClose(var T: TextRec); forward;
procedure UartRead(var T: TextRec); forward;
procedure UartWrite(var T: TextRec); forward;


function LoadDriver: THandle;
begin
  if HDriver = 0 then
    HDriver := kos_loaddriver('UART'); { @xxx: обязательно в UpperCase, т.е. регистр должен соотв.
                                               регистру имени драйвера или драйвер будет загружаться повторно,
                                               так работает сравнение имён в ядре (см. dll/get_service)!!! }
  Result := HDriver;
end;

procedure ReplaceStdIOWithUart(const Port: Byte);
begin
  Close(Input);
  Close(Output);
  Close(ErrOutput);
  Close(StdOut);
  Close(StdErr);

  AssignUART(Input,  Port);    Reset(Input);
  AssignUART(Output, Port);    Append(Output);
  AssignUART(ErrOutput, Port); Append(ErrOutput);
  AssignUART(StdOut, Port);    Append(StdOut);
  AssignUART(StdErr, Port);    Append(StdErr);
end;

procedure AssignUART(out T: Text; const Port: Byte);
begin
  FillChar(T, SizeOf(TextRec), 0);

  with TextRec(T) do
  begin
    Handle := UnusedHandle;
    Mode   := fmClosed;
    Name   := '/dev/uart' + IntToStr(Port);

    case DefaultTextLineBreakStyle of
      tlbsLF  : LineEnd := #10;
      tlbsCRLF: LineEnd := #13#10;
      tlbsCR  : LineEnd := #13;
    end;

    BufSize := TextRecBufSize;
    BufPtr  := @Buffer;

    PUartData(@UserData)^.Port := Port;

    OpenFunc  := @UartOpen;
    CloseFunc := @UartClose;
  end;
end;

procedure UartOpen(var T: TextRec);
var
  Port   : DWord;
  HDriver: THandle;
  HPort  : THandle;
  UData  : PUartData;
begin
{ WriteLn(StdErr, '[UartOpen]');}

  if T.Mode = fmAppend then
    T.Mode := fmOutput;

  if T.Mode = fmInput then
  begin
    HDriver := LoadDriver;
    if HDriver = 0 then
    begin
      InOutRes := 152;
      Exit;
    end;
    T.InoutFunc := @UartRead;
  end else

  if T.Mode = fmOutput then
  begin
    HDriver := LoadDriver;
    if HDriver = 0 then
    begin
      InOutRes := 152;
      Exit;
    end;
    T.InoutFunc := @UartWrite;
    T.FlushFunc := @UartWrite;
  end else
  begin
    InOutRes := 102;
    Exit;
  end;

  UData := PUartData(@T.UserData);
  Port  := UData^.Port;

  if (Low(HPorts) <= Port) and (Port <= High(HPorts)) and (0 < HPorts[Port].Counter) then
  begin
    Inc(HPorts[Port].Counter);
    UData^.Control.Handle := HDriver;
    UData^.HPort := HPorts[Port].Handle;
    T.Handle := DWord(UData);
    InOutRes := 0;
  end else
  begin
    with UData^.Control do
    begin
      Handle     := HDriver;
      Func       := PORT_OPEN;
      Data       := @Port;
      DataSize   := SizeOf(Port);
      Result     := @HPort;
      ResultSize := SizeOf(HPort);
    end;

    if kos_controldriver(@UData^.Control) < 0 then
      InOutRes := 152 else
    begin
      HPorts[Port].Handle := HPort;
      Inc(HPorts[Port].Counter);
      UData^.HPort := HPort;
      T.Handle := DWord(UData);
      InOutRes := 0;
    end;
  end;

{ WriteLn(StdErr, '- InOutRes: ', InOutRes);
  WriteLn(StdErr, '- T.Handle: ', IntToStr(T.Handle), ' ($', IntToHex(T.Handle, 8), ')');
  WriteLn(StdErr, '- HPort   : ', IntToStr(UData^.HPort), ' ($', IntToHex(UData^.HPort, 8), ')');}

  with UData^.Control do
  if T.Mode = fmInput then
    Func       := PORT_READ else
  begin
    Func       := PORT_WRITE;
    Result     := nil;
    ResultSize := 0;
  end;
end;

procedure UartClose(var T: TextRec);
var
  UData : PUartData;
  Port  : DWord;
begin
  T.Handle := UnusedHandle;

  UData := PUartData(@T.UserData);
  Port  := UData^.Port;

  if (Low(HPorts) <= Port) and (Port <= High(HPorts)) and (0 < HPorts[Port].Counter) then
  begin
    Dec(HPorts[Port].Counter);
    if 0 < HPorts[Port].Counter then
      Exit;
  end;

  with UData^.Control do
  begin
    Func       := PORT_CLOSE;
    Data       := @UData^.HPort;
    DataSize   := SizeOf(UData^.HPort);
    Result     := nil;
    ResultSize := 0;
  end;

  if kos_controldriver(@UData^.Control) < 0 then
    InOutRes := 152;

  T.Handle := UnusedHandle;
end;

procedure UartRead(var T: TextRec);
var
  UData : PUartData;
  Buffer: TUartPortBuffer;
  I, Got: Longint;
{ Chunk : String;}
begin
  UData := PUartData(@T.UserData);

  Buffer.Port    := UData^.HPort;
  Buffer.Address := T.BufPtr;
  Buffer.Size    := T.BufSize;

  with UData^.Control do
  begin
    Data       := @Buffer;
    DataSize   := SizeOf(Buffer);
    Result     := @Got;
    ResultSize := SizeOf(Got);
  end;

  while True do
  begin
    if (kos_controldriver(@UData^.Control) < 0) or (Got < 0) or (T.BufSize < Got) then
    begin
      InOutRes := 100;
      Exit;
    end;
    if 0 < Got then Break;
    Sleep(20);
  end;

{ SetString(Chunk, PChar(T.BufPtr), Got);
  WriteLn(StdErr, 'UartRead: ', Got, ' ', IntToHex(PDWord(T.BufPtr)^, 8));
  if PChar(T.BufPtr)^ = #13 then PChar(T.BufPtr)^ := #10;}

  { @xxx: [bw] QEMU в Linux шлёт одинарный #13, с которым не работает ReadLn,
          на реальном железе ввод не работает вообще, нужно смотреть драйвер }
  for I := 0 to Got - 1 do
  if T.BufPtr^[I] = #13 then T.BufPtr^[I] := #10;

  T.BufPos := 0;
  T.BufEnd := Got;
end;

procedure UartWrite(var T: TextRec);
var
  UData : PUartData;
  Buffer: TUartPortBuffer;
begin
  if T.BufPos = 0 then
    Exit;

  UData := PUartData(@T.UserData);

  Buffer.Port    := UData^.HPort;
  Buffer.Address := T.BufPtr;
  Buffer.Size    := T.BufPos;

  with UData^.Control do
  begin
    Data     := @Buffer;
    DataSize := SizeOf(Buffer);
  end;

{ Result := kos_controldriver(@Control);
  if Result < 0 then
  begin
    Sleep(10);
    Result := kos_controldriver(@Control);
  end;}

  if kos_controldriver(@UData^.Control) < 0 then
    InOutRes := 101;

  T.BufPos := 0;
end;


initialization
  Assert(SizeOf(TUartData) <= SizeOf(TextRec.UserData));
end.
