{$codepage utf8}

unit System;

{$define DISABLE_NO_THREAD_MANAGER} {???}

{$ifdef DEBUG}
  {$define DEBUG_MT}
  {$define SYSTEMDEBUG}
  {-$define DUMP_MEM_USAGE}
{$endif}

{-$ifdef FPC_HAS_FEATURE_VARIANTS}
{-$endif}

interface


{$i systemh.inc}

{$i kos_def.inc}
{$i kosh.inc}


const
  LineEnding = #13#10;
  LFNSupport = True;
  DirectorySeparator = '/';
  DriveSeparator = '/';
  ExtensionSeparator = '.';
  PathSeparator = ':';
  AllowDirectorySeparators: set of Char = ['\', '/'];
  AllowDriveSeparators: set of Char = [];
  MaxExitCode  = 255;
  MaxPathLen   = 260;
  AllFilesMask = '*';

  UnusedHandle   : THandle = -1;
  StdInputHandle : THandle = 0;
  StdOutputHandle: THandle = 1;
  StdErrorHandle : THandle = 2;
  FileNameCaseSensitive: Boolean = True;
  CtrlZMarksEOF: Boolean = False;
  sLineBreak = LineEnding;
  DefaultTextLineBreakStyle: TTextLineBreakStyle = tlbsCRLF;

var
  Argc: Longint = 0;
  Argv: PPChar = nil;
  EnvP: PPChar = nil;


function KosFileToRunError(ErrNo: DWord): Word;

function KonsoleGetThreadID: TThreadID;


implementation


{$i system.inc}

procedure KonsoleStartMaster; external name 'KonsoleStartMaster';
procedure KonsoleStartSlave(const ThreadID: TThreadID); external name 'KonsoleStartSlave';
function  KonsoleGetThreadID: TThreadID; external name 'KonsoleGetThreadID';
function  KonsoleAssignStdIO: Boolean; external name 'KonsoleAssignStdIO';


procedure SetupCmdLine;
var
  Ptrs: array of PChar;
  Args: PChar;
  I, L: Longint;
  Path: PChar;
  Utf8: UTF8String;
  AStr: String;
begin
  Argc := 1;
  Args := PKosHeader(0)^.args;

  { нулевой аргумент это текущее приложение }
  SetLength(Ptrs, Argc);

  Path := PKosHeader(0)^.path;

  { @xxx: Кажется это костыль для юникода в путях:
          - '/hd0/...'      -- OK!  5088,5343,6021
          - '/'#3'/hd0/...' -- WTF? 6822,7140,8329
    @see: (?) http://wiki.kolibrios.org/wiki/SysFn70/ru -- UTF-8 }

  if (Path[0] = '/') and (Path[1] < #32) then
  begin
    { @xxx: [bw] похуй }
    if Path[1] = #2 then
    begin
      WriteLn(StdErr, 'UTF-16 in path not supported');
      RunError(255);
    end else
    if Path[1] = #3 then
    begin
      Utf8 := StrPas(@Path[2]);
      AStr := Utf8ToAnsi(Utf8);
      Move(AStr[1], Path[2], Length(AStr));
    end;
    Inc(Path, 2);
  end;

  Ptrs[0] := Path;

  if Assigned(Args) then
  while Args^ <> #0 do
  begin
    { пропустить лидирующие пробелы }
    while Args^ in [#1..#32] do Inc(Args);
    if Args^ = #0 then Break;

    { запомнить указатель на параметр }
    Inc(Argc);
    SetLength(Ptrs, Argc);
    Ptrs[Argc - 1] := Args;

    { пропустить текущий параметр }
    while not (Args^ in [#0..#32]) do Inc(Args);

    { установить окончание параметра (@xxx: портим заголовок!!!) }
    if Args^ in [#1..#32] then
    begin
      Args^ := #0;
      Inc(Args);
    end;
  end;

  { настроить указатели argv на args (прямо в заголовок) }
  Argv := GetMem(Argc * SizeOf(PChar));
  L := 0;
  for I := 0 to Argc - 1 do
  begin
    Argv[I] := Ptrs[I];
    Inc(L, Length(Argv[I]));
  end;

  { собрать cmdline }
  if 0 < Argc then
  begin
    CmdLine := GetMem(L + Argc);
    L := 0;
    for I := 0 to Argc - 1 do
    begin
      Move(Argv[I]^, CmdLine[L], Length(Argv[I]));
      Inc(L, Length(Argv[I]));
      if I = Argc - 1 then
        CmdLine[L] := #0 else
        CmdLine[L] := ' ';
      Inc(L, 1);
    end;
  end else
    CmdLine := nil;
end;

function ParamCount: Longint;
begin
  Result := Argc - 1;
end;

function ParamStr(L: Longint): String;
begin
  if (L >= 0) and (L < Argc) then
    Result := StrPas(Argv[L]) else
    Result := '';
end;

procedure Randomize;
begin
  randseed := kos_timecounter();
end;

const
  ProcessID: SizeUInt = 0;

function GetProcessID: SizeUInt;  { @todo: GetProcessID }
begin
  GetProcessID := ProcessID;
end;

{$i kos_stdio.inc}

procedure SysInitStdIO;
begin
  { @note: перечисленные ниже переменные являются `threadvar`-ами (см. `rtl/inc/systemh.inc`),
           эта процедура так же вызывается из `InitThread` (см. `rtl/inc/thread.inc`) }

  if not KonsoleAssignStdIO then
  begin
    AssignStdin(Input);
    AssignStdout(Output);
    AssignStdout(ErrOutput);
    AssignStdout(StdOut);
    AssignStdout(StdErr);
  end;
end;

procedure System_Exit; public name 'SystemExit';
begin
  { @note: (!) эта штука вызывается после `ExitProc`-ов, `FinalizeUnits` и `FinalizeHeap` }
  { @see: `Halt`, `InternalExit` и `FinalizeUnits`}

  if ExitCode <> 0 then
    WriteLn(StdErr, '[Error #', ExitCode, ']');

  Close(StdErr);
  Close(StdOut);
  Close(ErrOutput);
  Close(Output);
  Close(Input);

  Argc := 0;
  if Assigned(Argv)    then begin FreeMem(Argv);    Argv    := nil; end;
  if Assigned(CmdLine) then begin FreeMem(CmdLine); CmdLine := nil; end;

  asm
    movl $-1, %eax
    int $0x40
  end;
end;

procedure FinalInit;
begin
  { Вызывается после инициализации всех модулей. }
end;

{$i kos.inc}
{$i kos_xpc.inc}

function CheckInitialStkLen(StkLen: SizeUInt): SizeUInt;
begin
  with PKosHeader(nil)^ do
  begin
    Result := Size;
    if Assigned(Args) and (Result < DWord(Args)) then
      Result := DWord(Args);
    if Assigned(Path) and (Result < DWord(Path)) then
      Result := DWord(Path);
    Result := Stack - Result;
  end;

  if StkLen < Result then
    Result := StkLen;
end;

{procedure SetupCurrentDir;
var
  Cwd: String;
begin
  Cwd := GetKosXpcData('parent.cwd');
  if Cwd <> '' then ChDir(Cwd);
end;}


var
  InitialStkPtr: Pointer; public name '__stkptr';
  XpcDataPtr   : Pointer; public name '__xpcptr';

begin
{$ifndef FPUNONE}
  SysResetFPU;
  if not IsLibrary then
    SysInitFPU;
{$endif}

  StackLength := CheckInitialStkLen(InitialStkLen);
  StackTop    := InitialStkPtr;
  StackBottom := Pointer(StackTop - StackLength);

  kos_initheap;
  InitHeap;
  SysInitExceptions;

  FPC_CpuCodeInit;
  InOutRes := 0;
  InitSystemThreads;
  ProcessID := GetThreadID;
  SysInitStdIO;

{$ifdef VER2_2}
  InitWideStringManager;
{$else VER2_2}
  InitUnicodeStringManager;
{$endif VER2_2}

  SetupCmdLine;
  InitVariantManager;
  DispCallByIDProc := @DoDispCallByIDError;

  SetupXpcData(XpcDataPtr);
{ SetupCurrentDir;}
  SetupEnvironment;

  InitProc := @FinalInit;

  { Дальше будет вызван `FPC_INITIALIZEUNITS`, а за тем `InitProc`. }
end.
