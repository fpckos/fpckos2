{-$codepage utf8}

{$macro on}
{$ifdef DEBUG}
  {$define WriteDebug := WriteLn}
{$else}
  {$define WriteDebug := //}
{$endif}

type
  PKonsole = ^TKonsole;
  TKonsole = object
  private
    FCaption   : String;
    FLines     : array of ShortString;
    FCursor    : TKosPoint;
    FMaxLines  : Word;
    FThreadID  : TThreadID;
    FThreadSlot: TThreadSlot;
    FIPCBuffer : PKosIPC;
    FIPCBufferSize: DWord;
    FMaster    : Boolean;
    FTerminate : Boolean;
    FOpened    : Boolean;
    FOnAir     : Boolean;
    FMinimized : Boolean;
    FKeyPressed: Word;
    FCaptionHeight: Longint;
    FWindowHeight : Longint;
    FBorderWidth  : Longint;
    FFontWidth    : Longint;
    FFontHeight   : Longint;
    function ReceiveMessage(var Message: ShortString): Boolean;
    procedure HandleMessage(Message: ShortString);
    procedure HandleKeyboard(const Key: Word);
    procedure HandleButton(const Button: DWord);
    function GetRect: TKosRect;
    function GetKeyPressed: Word;
    function GetAlive: Boolean;
    procedure SetCaption(const Value: String);
    procedure Paint;
    procedure Paint(BottomRow: Boolean);
    procedure PaintCaption;
    procedure Flush;
  public
    constructor Master;
    constructor Master(ACaption: String);
    constructor Slave(const ThreadID: TThreadID);
    destructor Done;
    procedure WaitForTerminate;
    procedure Write(const Message: ShortString);
    property KeyPressed: Word read GetKeyPressed;
    property Alive: Boolean read GetAlive;
    property Opened: Boolean read FOpened;
    property Caption: String read FCaption write SetCaption;
    property ThreadID: TThreadID read FThreadID;       {JustForFun, must be hidden, do not use}
    property ThreadSlot: TThreadSlot read FThreadSlot; {JustForFun, must be hidden, do not use}
  end;


var
  Konsole: PKonsole;


procedure AssignKonsoleOutput(var f: Text); forward;
procedure AssignKonsoleInput(var f: Text); forward;


procedure KonsoleOnExit;
var
  S: String;
begin
  if Assigned(Konsole) then
  begin
    Str(ExitCode, S);
    Konsole^.Caption := Konsole^.Caption + ' [exit:' + S + ']';
  end;
end;

procedure KonsoleStartMaster; public name 'KonsoleStartMaster';
begin
  if not Assigned(Konsole) then
  begin
    New(Konsole, Master);
    AddExitProc(@KonsoleOnExit);
{$ifdef DEBUG}
    Konsole^.Write('Hello from Master...'#13#10);}
{$endif}
  end;
end;

procedure KonsoleStartSlave(const ThreadID: TThreadID); public name 'KonsoleStartSlave';
begin
  if not Assigned(Konsole) then
  begin
    New(Konsole, Slave(ThreadID));
{$ifdef DEBUG}
    Konsole^.Write('Hello from Slave...'#13#10);
{$endif}
  end;
end;

procedure KonsoleStop; public name 'KonsoleStop';
begin
  if Assigned(Konsole) then
    Dispose(Konsole, Done);
end;

procedure KonsoleWait; public name 'KonsoleWait';
begin
  if Assigned(Konsole) then
    Konsole^.WaitForTerminate;
end;

function KonsoleGetThreadID: TThreadID; public name 'KonsoleGetThreadID';
begin
  if Assigned(Konsole) then
    Result := Konsole^.ThreadID else
    Result := -1;
end;

function KonsoleAssignStdIO: Boolean; public name 'KonsoleAssignStdIO';
begin
  if Assigned(Konsole) then
  begin
   {Close(Input);}
    Close(Output);    AssignKonsoleOutput(Output);
    Close(StdOut);    AssignKonsoleOutput(StdOut);
    Close(ErrOutput); AssignKonsoleOutput(ErrOutput);
    Close(StdErr);    AssignKonsoleOutput(StdErr);
    Result := True;
  end else
    Result := False;
end;

{------------------------------------------------------------------------------}

{$i textrec.inc}

procedure OpenStdout(var f: TextRec); forward;
procedure WriteStdout(var f: TextRec); forward;
procedure FlushStdout(var f: TextRec); forward;
procedure CloseStdout(var f: TextRec); forward;

procedure OpenStdin(var f: TextRec); forward;
procedure ReadStdin(var f: TextRec); forward;
procedure CloseStdin(var f: TextRec); forward;


procedure AssignKonsoleOutput(var f: Text);
begin
  Assign(f, '');
  TextRec(f).OpenFunc := @OpenStdout;
  Rewrite(f);
end;

procedure OpenStdout(var f: TextRec);
begin
  TextRec(f).InOutFunc := @WriteStdout;
  TextRec(f).FlushFunc := @FlushStdout;
  TextRec(f).CloseFunc := @CloseStdout;
end;

procedure WriteStdout(var f: TextRec);
var
  S: ShortString;
  L: Integer;
begin
  while f.bufpos > 0 do
  begin
    if 255 < f.bufpos then
      L := 255 else
      L := f.bufpos;
    SetString(S, PChar(f.bufptr), L);
    Konsole^.Write(S);
    Dec(f.bufpos, L);
  end;
end;

procedure FlushStdout(var f: TextRec);
begin
  WriteStdout(f);
end;

procedure CloseStdout(var f: TextRec);
begin
end;

procedure AssignKonsoleInput(var f: Text);
begin
  Assign(f, '');
  TextRec(f).OpenFunc := @OpenStdin;
  Reset(f);
end;

procedure OpenStdin(var f: TextRec);
begin
  TextRec(f).InOutFunc := @ReadStdin;
  TextRec(f).FlushFunc := nil;
  TextRec(f).CloseFunc := @CloseStdin;
end;

procedure ReadStdin(var f: TextRec);
begin
  { @todo: ReadStdin }
  f.bufpos := 0;
  f.bufend := 0;
end;

procedure CloseStdin(var f: TextRec);
begin
end;


{------------------------------------------------------------------------------}

procedure KonsoleThreadMain(Konsole: PKonsole);
{ Рабочий цикл консоли. }
var
  ThreadInfo: TKosThreadInfo;
  Message: ShortString;
  Event: DWord;
begin
  kos_maskevents(ME_PAINT or ME_KEYBOARD or ME_BUTTON or ME_IPC);
  kos_threadinfo(@ThreadInfo);
  Konsole^.FThreadSlot := kos_getthreadslot(ThreadInfo.ThreadID);

  kos_initipc(Konsole^.FIPCBuffer, Konsole^.FIPCBufferSize);

  { сразу отобразить и активировать окно }
  Konsole^.Paint();
  {$ifndef EMULATOR}
  kos_setactivewindow(Konsole^.FThreadSlot);
  {$endif}

  { готов к обработке событий }
  Konsole^.FOpened := True;

  try
    while not Konsole^.FTerminate do
    begin
      Event := kos_getevent();
      Konsole^.FOnAir := True;

      if not Konsole^.FTerminate then
      case Event of
        SE_PAINT   : Konsole^.Paint;
        SE_KEYBOARD: Konsole^.HandleKeyboard(kos_getkey());
        SE_BUTTON  : Konsole^.HandleButton(kos_getbutton());
        SE_IPC     : while Konsole^.ReceiveMessage(Message) do Konsole^.HandleMessage(Message);
      end;

      Konsole^.FOnAir := False;
    end;
  finally
    Konsole^.FOnAir  := False;
    Konsole^.FOpened := False;
  end;
end;


constructor TKonsole.Master;
const
  IPC_SIZE = 4096;
var
  ThreadInfo: TKosThreadInfo;
  I: Integer;
begin
  FMaster := True;

  if FCaption = '' then
  begin
    kos_threadinfo(@ThreadInfo);
    FCaption := StrPas(ThreadInfo.AppName);
  end;

  SetLength(FLines, 1);
  FLines[0] := '';

  FCaptionHeight := 16;
  FBorderWidth   := 5;
  FFontWidth     := 6;
  FFontHeight    := 9;
  FWindowHeight  := 400;
  FCursor.X := 1;
  FCursor.Y := 0;
  FMaxLines := 150;
  FTerminate := False;

  FOpened := False;
  FOnAir  := False;

  FIPCBufferSize := SizeOf(TKosIPC) + IPC_SIZE;
  FIPCBuffer := GetMem(FIPCBufferSize);
  FIPCBuffer^.Lock := False;
  FIPCBuffer^.Size := 0;

  FThreadSlot := -1;
  BeginThread(nil, DefaultStackSize, TThreadFunc(@KonsoleThreadMain), @Self, THREAD_NODAEMON, FThreadID);

  if FThreadID <> 0 then
  for I := 1 to 20 do  { 2.0 sec }
  begin
    if FOpened then Break;
    kos_delay(10);     { 0.1 sec }
    ThreadSwitch;
  end;
  if not FOpened then Halt(210);
end;

constructor TKonsole.Master(ACaption: String);
begin
  FCaption := ACaption;
  Master;
end;

constructor TKonsole.Slave(const ThreadID: TThreadID);
begin
  FMaster     := False;
  FThreadID   := ThreadID;
  FThreadSlot := kos_getthreadslot(ThreadID);
  FOpened     := FThreadSlot <> 0;
  if not FOpened then Halt(210);
end;

destructor TKonsole.Done;
begin
  FTerminate := True;
  if FOpened then begin Self.Write(#0); kos_delay(1); end;
  if FOpened then begin Self.Write(#0); kos_delay(10); end;
  if FOpened then begin Self.Write(#0); kos_delay(20); end;
  if FOpened then
  begin
    FOpened := False;
    FOnAir  := False;
    KillThread(FThreadID);
  end;
  {FreeMem(FIPCBuffer);
  SetLength(FLines, 0);}
end;

function TKonsole.ReceiveMessage(var Message: ShortString): Boolean;
{ Извлечь первое сообщение из буфера. }
var
  PMsg: PKosMessage;
  Size: Longword;
begin
  FIPCBuffer^.Lock := True;

  if FIPCBuffer^.Size > 0 then
  begin
    PMsg := Pointer(Longword(FIPCBuffer) + SizeOf(TKosIPC));
    {TODO: проверка PMsg^.SenderID}
    {Size := PMsg^.Size;
    Dec(FIPCBuffer^.Size, Size + SizeOf(TKosMessage));
    if Size > 255 then Size := 255;
    SetLength(Message, Size);
    Move(Pointer(Longword(PMsg) + SizeOf(TKosMessage))^, Message[1], Size);
    if FIPCBuffer^.Size > 0 then
      Move(Pointer(Longword(PMsg) + SizeOf(TKosMessage) + PMsg^.Size)^, PMsg^, FIPCBuffer^.Size);}

    {XXX}
    Size := FIPCBuffer^.Size;
    Dec(FIPCBuffer^.Size, Size);
    if Size > 255 then Size := 255;
    SetLength(Message, Size);
    Move(PMsg^, Message[1], Size);

    ReceiveMessage := True;
  end else
  begin
    Message := '';
    ReceiveMessage := False;
  end;

  {FIXME: если FIPCBuffer^.Size = 0, то FIPCBuffer^.Lock все равно > 0}
  FIPCBuffer^.Lock := False;
end;

procedure TKonsole.HandleMessage(Message: ShortString);
{ Вывести сообщение на консоль }
{ @todo: (!) оптимизировать перерисовку }
var
  OnlyBottomLine: Boolean = True;
  Repaint       : Boolean = False;

  procedure PutChar(C: Char);
  var
    LinesCount: Longint;
    PLine: PShortString;
    I: Longint;
  begin
    { передёрнуть заголовок }
    if C = #1 then
      PaintCaption else

    { перевод каретки на позицию влево }
    if C = #8 then
    begin
      Repaint := True;
      if FCursor.X > 1 then
        Dec(FCursor.X);
    end else

    { перевод каретки на следующую строку }
    if C = #10 then
    begin
      Repaint := True;
      OnlyBottomLine := False;
      Inc(FCursor.Y);
      LinesCount := Length(FLines);
      while FCursor.Y >= FMaxLines do Dec(FCursor.Y, FMaxLines);
      if FCursor.Y < LinesCount then FLines[FCursor.Y] := '';
      while FCursor.Y >= LinesCount do
      begin
        SetLength(FLines, LinesCount + 1);
        FLines[LinesCount] := '';
        Inc(LinesCount);
      end;
    end else

    { перевод каретки в начало строки }
    if C = #13 then
    begin
      Repaint := True;
      FCursor.X := 1;
    end else

    { помещение символа в строку }
    begin
      if FCursor.X > 200 then
      begin
        PutChar(#13);
        PutChar(#10);
      end;

      { @fixme: [2007] Если в PascalMain только один Write/Ln, то зависон.
                       см. FPC_DO_EXIT, InternalExit }
      PLine := @FLines[FCursor.Y];
      I := Length(PLine^);
      if FCursor.X > I then
      begin
        SetLength(PLine^, FCursor.X);
        Inc(I);
        while I < FCursor.X do
        begin
          PLine^[I] := ' ';
          Inc(I);
        end;
      end;
      FLines[FCursor.Y][FCursor.X] := C;

      Inc(FCursor.X);
    end;
  end;

var
  I: Longint;
begin
  for I := 1 to Length(Message) do
    PutChar(Message[I]);

  if Repaint and not FMinimized then
    Paint(OnlyBottomLine);
end;

procedure TKonsole.HandleKeyboard(const Key: Word);
var
  CtrlKey: Word;
begin
  { @see: http://wiki.kolibrios.org/wiki/SysFn02/ru }
  { @see: http://wiki.kolibrios.org/wiki/SysFn66/ru }
  CtrlKey := kos_getcontrolkeys();  { LShift=01b RShift=10b }
  WriteDebug('[HandleKeyboard] ', HexStr(Key, 4), ' (', HexStr(CtrlKey, 2), ')');
  { q=$7100 Q=$5100 ^q=$1100 ^Q=$5100
    й=$A900 Й=$8900 ^й=$4900 ^Й=$8900 (cp866)
    PgUp=$B800 PgDn=$B700 S+PgUp=$B800 S+PgDn=$B700 ^PgUp=$5800 ^PgDn=$5700 }
  if Key and $ffff00ff = 0 then
  if (Key = $1100) or (Key = $4900) then
    FTerminate  := True else
  if (Key = $b800) then { @todo: PgUp } else
  if (Key = $b700) then { @todo: PgDn } else
    FKeyPressed := Key;
end;

procedure TKonsole.HandleButton(const Button: DWord);
begin
  WriteDebug('[HandleButton] ', Button);
  if 1 < Button then
  case Button shr 8 of
    BTN_CLOSE   : FTerminate := True;
   {BTN_MINIMIZE: FMinimized := True;}
  end;
end;

function TKonsole.GetRect: TKosRect;
var
  ThreadInfo: TKosThreadInfo;
begin
  kos_threadinfo(@ThreadInfo, FThreadSlot);
  GetRect := ThreadInfo.WindowRect;
end;

function TKonsole.GetKeyPressed: Word;
begin
  GetKeyPressed := FKeyPressed;
  FKeyPressed := 0;
end;

function TKonsole.GetAlive: Boolean;
var
  ThreadInfo: TKosThreadInfo;
begin
  GetAlive := (kos_threadinfo(@ThreadInfo, FThreadSlot) = 0)
          and (ThreadInfo.ThreadID = FThreadID)
          and (ThreadInfo.Status <> KOS_THREAD_FREE_SLOT);
end;

procedure TKonsole.SetCaption(const Value: String);
begin
  if FMaster then
  begin
    FCaption := Value;
    if GetCurrentThreadID <> FThreadID then
      Write(#1) else
      PaintCaption;
  end;
end;

procedure TKonsole.Paint;
begin
  Paint(False);
end;

procedure TKonsole.Paint(BottomRow: Boolean);
var
  Rect: TKosRect;
  J: Longint;
  Width, Height, Row: Longint;
begin
  if not FMaster then Exit;

  FMinimized := False;

  kos_begindraw;

  if not BottomRow then
  begin
    { отрисовка окна }
    kos_definewindow(60, 60, 400, FWindowHeight, $63000000);
    PaintCaption;
  end;

  { подготовка к выводу строк }
  Rect := GetRect;
  Dec(Rect.Width, FBorderWidth * 2);
  Dec(Rect.Height, FCaptionHeight + FBorderWidth * 2);
  Width  := Rect.Width div FFontWidth;
  Height := Rect.Height - FFontHeight;
  Row := FCursor.Y;

  while Height > 0 do
  begin
    { вывод одной строки }
    J := Length(FLines[Row]);
    if J > Width then J := Width;
    kos_drawtext(0, Height, Copy(FLines[Row], 1, J), $00DD00, $FF000000);
    { заливка оставшегося пространства в строке }
    J := J * FFontWidth;
    kos_drawrect(J, Height, Rect.Width - J + 1, FFontHeight, $000000);
    { подготовка к выводу следующей строки }
    Dec(Height, FFontHeight);
    Dec(Row);
    if BottomRow or ((Row < 0) and (Length(FLines) < FMaxLines)) then Break;
    while Row < 0 do Inc(Row, FMaxLines);
  end;

  if FCursor.X <= Width then
    { отрисовка курсора }
    kos_drawrect((FCursor.X - 1) * FFontWidth, Rect.Height - 2, FFontWidth, 2, $FFFFFF);

  if not BottomRow then
    { заливка оставшейся части окна }
    kos_drawrect(0, 0, Rect.Width + 1, Height + FFontHeight, $000000);

  kos_enddraw;
end;

procedure TKonsole.PaintCaption;
var
  Buffer: array[Byte] of Char;
begin
  if Length(FCaption) < Length(Buffer) then
  begin
    Move(FCaption[1], Buffer, Length(FCaption));
    Buffer[Length(FCaption)] := #0;
  end else
  begin
    { @warn: unreachable code }
    Move(FCaption[1], Buffer, Length(Buffer) - 1);
    Buffer[Length(Buffer) - 1] := #0;
  end;
  kos_setcaption(Buffer);
end;

procedure TKonsole.Write(const Message: ShortString);
var
  I: Integer;
begin
  { @xxx: возможна ситуация при которой сообщение не будет отправлено }
  { @xxx: есть спец.коды, например #1 }
  if FOpened then
  begin
    I := 100;
    while (kos_sendmsg(FThreadID, @Message[1], Length(Message)) = 2) and (I > 0) do
    begin
      Dec(I);
      ThreadSwitch;
    end;
  end;
end;

procedure TKonsole.Flush;
begin
  while Alive and FOnAir do ThreadSwitch;
end;

procedure TKonsole.WaitForTerminate;
begin
  WaitForThreadTerminate(FThreadID, 0);
end;
