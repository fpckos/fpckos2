{TODO}

function SysOSAlloc(Size: PtrInt): Pointer;
begin
  Result := kos_alloc(Size);
end;

{$define HAS_SYSOSFREE}
procedure SysOSFree(P: Pointer; Size: PtrInt);
begin
  kos_free(P);
end;


(*
{ == DEBUG Version ==== }

var
  SysMemoryBlocks: array[Word] of record
    Used: Boolean;
    Address: Pointer;
    Size: Longint;
  end;


procedure ShowMemoryInfo(ActionName: String; Index, Size: DWord);
var
  I, C: DWord;
begin
  C := 0;
  for I := Low(SysMemoryBlocks) to High(SysMemoryBlocks) do
  if SysMemoryBlocks[I].Used then
    Inc(C);
  WriteLn('[MEM] ', ActionName, ' | Index: ', Index, ' | Size: ', Size, ' | Used Blocks: ', C);
end;


function SysOSAlloc(Size: PtrInt): Pointer;
var
  I: DWord;
begin
  Result := kos_alloc(Size);

  for I := Low(SysMemoryBlocks) to High(SysMemoryBlocks) do
  if not SysMemoryBlocks[I].Used then
  begin
    SysMemoryBlocks[I].Used := True;
    SysMemoryBlocks[I].Address := Result;
    SysMemoryBlocks[I].Size := Size;

    ShowMemoryInfo('Alloc', I, Size);
    Break;
  end;
end;


{$define HAS_SYSOSFREE}
procedure SysOSFree(P: Pointer; Size: PtrInt);
var
  B: Byte;
  I: DWord;
begin
  B := 0;
  for I := Low(SysMemoryBlocks) to High(SysMemoryBlocks) do
  if SysMemoryBlocks[I].Address = P then
  begin
    SysMemoryBlocks[I].Used := False;
    if SysMemoryBlocks[I].Size <> Size then B := 1 div B;

    ShowMemoryInfo('Free', I, Size);
    Break;
  end;

  kos_free(P);
end;
*)
