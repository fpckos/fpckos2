
procedure SetEnvironment(Name: String; const Value: String); forward;
procedure UpdateEnvorinmentFromFile(const FileName: String); forward;


function GetKosXpcData(const Name: String): String;
var
  I: Integer;
begin
  for I := Low(KosXpcData) to High(KosXpcData) do
  if KosXpcData[I].Name = Name then
  begin
    Result := KosXpcData[I].Value;
    Exit;
  end;
  Result := '';
end;


function InjectKosXPC(Pid: DWord; Size: DWord; Data: Pointer): Boolean;
const
  Magic: array[1..12] of Char = #0#0#0#0'XPC00'#0#0#0;
var
  I, R   : Longint;
  Content: TKosThreadContent;
  SizeW  : Word;
  SizeL  : Longword;

{$if SizeOf(Content) <> 40}
  {$fatal Unexpected type size}
{$endif}

begin
  if 262144 <= Size then  { 65536 * 4 }
  begin
    kos_debugkill(Pid);
    Result := False;
    Exit;
  end;

  kos_debuggetregisters(Pid, @Content);

  SizeW := (Size + 3) div 4;
  SizeL := SizeW * 4;

 {S := StrPas(PChar(Data));
  WriteLn('[InjectKosXPC] ESP=', HexStr(Content.esp, 8), ' (', Size, ') ', S);}

  for I := 1 to 3 do
  begin
    ThreadSwitch;
    R := kos_debugwritememory(Pid, Length(Magic), @Magic[1], Pointer(Longint(Content.esp) - Length(Magic)));
    if R = Length(Magic) then
    begin
      Dec(R, 4);
      Inc(R, kos_debugwritememory(Pid, Size, Data, Pointer(Longint(Content.esp) - 8 - SizeL)));
      if kos_debugwritememory(Pid, SizeOf(SizeW), @SizeW, Pointer(Longint(Content.esp) - 2)) <> 2 then
        Dec(R, 2);
    end;
    if 0 < R then Break;
  end;

  if R <> (Length(Magic) + Size - 4) then
  begin
    kos_debugkill(Pid);
    { @todo: ??? }
    Result := False;
    Exit;
  end;

  kos_debugdetach(Pid);
  Result := True;
end;


procedure SetupXpcData(XpcDataPtr: Pointer);
var
  C    : PChar;
  Q, I : Integer;
  Name : String;
  Value: String;
begin
  if not Assigned(XpcDataPtr) then Exit;

  C := XpcDataPtr;

  while C^ <> #0 do
  begin
    if C^ = '"' then
    begin
      Q := 1;
      Inc(C);
    end else
      Q := 0;

    Value := '';

    while (C^ <> #0) and ((0 < Q) or (C^ <> ',')) do
    begin
      if (C^ = '"') and (Q = 1) then
      begin
        Inc(C);
        if C^ <> '"' then
        begin
          Dec(Q);
          Continue;
        end;
      end;
      Value := Value + C^;
      Inc(C);
    end;

    if C^ = ',' then Inc(C);

    if 0 < Length(Value) then
    begin
      I := Pos('=', Value);

      if 0 < I then
      begin
        Name  := Copy(Value, 1, I - 1);
        Value := Copy(Value, I + 1, Length(Value) - I);
      end else
      begin
        Name  := Value;
        Value := '';
      end;

      if Name[1] = '$' then
      begin
        SetLength(KosXpcData, Length(KosXpcData) + 1);
        KosXpcData[High(KosXpcData)].Name  := Copy(Name, 2, Length(Name) - 1);
        KosXpcData[High(KosXpcData)].Value := Value;
      end else
        SetEnvironment(Name, Value);
    end;
  end;
end;


procedure SetupEnvironment;

  function ExtractRootPath(const FileName: String): String;
  var
    I, C: Integer;
  begin
    { @ex: /rd/1/, /hd0/1/, /tmp0/1/ }
    { @xxx: /sys/ }
    if (Length(FileName) = 0) or (FileName[1] <> '/') then
      Result := '' else
    if (5 <= Length(FileName)) and (LowerCase(Copy(FileName, 1, 5)) = '/sys/') then
      Result := '/sys/' else
    begin
      I := 1;
      C := 1;
      while (C < 3) and (I < Length(FileName)) do
      begin
        Inc(I);
        if FileName[I] = '/' then Inc(C);
      end;
      if 1 < C then
      begin
        Result := Copy(FileName, 1, I);
        if C < 3 then Result := Result + '/';
      end;
    end;
  end;

var
  BasePath: String;
begin
  if Length(KosEnvironment) = 0 then
  begin
    UpdateEnvorinmentFromFile('/sys/settings/.environ');
    BasePath := ExtractRootPath(ParamStr(0));
    if BasePath <> '' then
      UpdateEnvorinmentFromFile(BasePath + '.environ');
  end;
end;


procedure SetEnvironment(Name: String; const Value: String);
var
  I: Integer;
begin
  Name := UpCase(Name);

  for I := Low(KosEnvironment) to High(KosEnvironment) do
  if KosEnvironment[I].Name = Name then
  begin
    KosEnvironment[I].Value := Value;
    Exit;
  end;

  SetLength(KosEnvironment, Length(KosEnvironment) + 1);
  KosEnvironment[High(KosEnvironment)].Name  := Name;
  KosEnvironment[High(KosEnvironment)].Value := Value;
end;


{$ifopt i+}
{$define do_open_iocheck_was_on}
{$i-}
{$endif}

procedure UpdateEnvorinmentFromFile(const FileName: String);

  function Trim(const S: String): String;
  const
    WhiteSpace = [#0..' '];
  var
    O, L: Integer;
  begin
    L := Length(S);
    O := 1;
    while (0 <  L) and (S[L] in WhiteSpace) do Dec(L);
    while (O <= L) and (S[O] in WhiteSpace) do Inc(O);
    Result := Copy(S, O, 1 + L - O);
  end;

  function ExtractFileDir(const FileName: String): String;
  var
    I     : Integer;
    EndSep: set of Char;
  begin
    I := Length(FileName);
    EndSep := AllowDirectorySeparators + AllowDriveSeparators;
    while (I > 0) and not (FileName[I] in EndSep) do Dec(I);
    if (I > 1) and (FileName[I] in AllowDirectorySeparators) and not (FileName[I - 1] in EndSep) then Dec(I);
    Result := Copy(FileName, 1, I);
  end;

  function GetValue(Name: String): String;
  var
    I: Integer;
  begin
    Name := UpCase(Name);

    if Name = '__HERE__' then
    begin
      Result := ExtractFileDir(FileName);
      Exit;
    end;

    for I := Low(KosEnvironment) to High(KosEnvironment) do
    if KosEnvironment[I].Name = Name then
    begin
      Result := KosEnvironment[I].Value;
      Exit;
    end;

    Result := '';
  end;

  function ApplySubstitution(const Value: String): String;
  const
    NameChars = ['0'..'9', 'a'..'z', 'A'..'Z', '_'];
  var
    I, S: Integer;
    E   : Boolean;
    C   : Char;
  begin
    Result := '';

    I := 0;
    S := 0;

    while I < Length(Value) do
    begin
      Inc(I);
      C := Value[I];

      if E then
      begin
        Result := Result + C;
        E := False;
      end else

      if 0 < S then
      begin
        if not (C in NameChars) then
        begin
          Result := Result + GetValue(Copy(Value, S + 1, I - S - 1)) + C;
          S := 0;
        end;
      end else

      if C = '\' then E := True else
      if C = '$' then S := I else
        Result := Result + C;
    end;

    if 0 < S then
      Result := Result + GetValue(Copy(Value, S + 1, I - S));
  end;

var
  F: Text;
  I: Integer;
  Name, Value: String;
begin
  Assign(F, FileName);
  Reset(F);
  if IOResult = 0 then
  begin
    while not Eof(F) do
    begin
      ReadLn(F, Value);
      Value := Trim(Value);

      if (0 < Length(Value)) and (Value[1] <> '#') then
      begin
        I := Pos('=', Value);

        if 0 < I then
        begin
          Name  := Trim(Copy(Value, 1, I - 1));
          Value := Trim(Copy(Value, I + 1, Length(Value) - I));
        end else
        begin
          Name  := Value;
          Value := '';
        end;

        SetEnvironment(Name, ApplySubstitution(Value));
      end;
    end;
    Close(F);
  end;
end;

{$ifdef do_open_iocheck_was_on}
{$undef do_open_iocheck_was_on}
{$i+}
{$endif}
