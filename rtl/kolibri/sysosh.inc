
const
  THREAD_NODAEMON = 1;  { такой поток не будет убиваться при завершении процесса (главного потока),
                          см. `BeginThread` и `DoneSystemThreads` в `systhrd.inc`; по-умолчанию потоки
                          убиваются }

type
  THandle   = Longint;
  TThreadID = Longint;
  TThreadSlot = Longint;
  UINT = Cardinal;
  BOOL = Longbool;
  ULONG_PTR = DWord;
  SIZE_T = ULONG_PTR;

  PRTLCriticalSection = ^TRTLCriticalSection;
  TRTLCriticalSection = packed record
    OwningThread: TThreadID;
  end;

  PKosRTLEvent = ^TKosRTLEvent;
  TKosRTLEvent = packed record
    Value  : Longint;
    Waiters: Longint;
  end;

var
  KosEnvironment: array of record Name, Value: String; end;
  KosXpcData    : array of record Name, Value: String; end;


function GetKosXpcData(const Name: String): String;
