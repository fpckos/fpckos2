{-$codepage utf8}

{$macro on}
{$ifdef DEBUG_MT}
  {$define WriteDebug := WriteLn}
{$else}
  {$define WriteDebug := //}
{$endif}

procedure TProcess.Execute;
var
  Path    : AnsiString;
  FullPath: AnsiString;
  Params  : AnsiString;
  DataL   : TStrings;
  DataS   : AnsiString;
  F       : File;
  KF      : PKosFile;
  Pid     : TThreadID;
  KoTid   : TThreadID;
  I       : Longint;

  function MakeError(ErrNo: Longint): EProcess;
  begin
    Result := EProcess.CreateFmt('Failed to execute "%s", error code: %d', [FCommandLine, ErrNo]);
  end;

  procedure WriteNotImplemented(const What: String);
  begin
    WriteLn(System.StdErr, '[TProcess] ' + What + ' not implemented');
  end;

begin
  if FCommandLine = '' then
    raise EProcess.Create(SNoCommandline);

  Assert(FStartupOptions = []);
  Assert(FCurrentDirectory = '');
  Assert(FEnvironment.Count = 0);
  Assert(FShowWindow = swoNone);

  Assert(not (csDesigning in ComponentState));
  Assert(not (poRunSuspended in FProcessOptions));

 {swoNone, swoHIDE, swoMaximize, swoMinimize, swoRestore, swoShow,
  swoShowDefault, swoShowMaximized, swoShowMinimized, swoshowMinNOActive,
  swoShowNA, swoShowNoActivate, swoShowNormal}
 {suoUseShowWindow, suoUseSize, suoUsePosition, suoUseCountChars, suoUseFillAttribute}

  DataL := TStringList.Create;

  if 0 < FEnvironment.Count then
    DataL.Assign(FEnvironment) else
  for I := 1 to GetEnvironmentVariableCount do
    DataL.Append(GetEnvironmentString(I));

  DataL.Append('$parent.pid=' + IntToStr(System.GetProcessID));
  DataL.Append('$parent.cwd=' + GetCurrentDir);

  KoTid := KonsoleGetThreadID;

  if 0 < KoTid then
    DataL.Append('$terminal.pid=' + IntToStr(KoTid));

  DataS := DataL.CommaText;
  DataL.Free;

  WriteDebug('[TProcess.Execute]');
  WriteDebug('CommandLine: "', FCommandLine, '"');
  WriteDebug('Data  : ', DataS);

  if 262144 <= Length(DataS) + 1 then  { 65536 * 4 }
    raise MakeError(-1);

{-$ifdef DEBUG_MT}
  if FProcessPriority <> ppNormal    then WriteNotImplemented('ProcessPriority');
  if FInheritHandles                 then WriteNotImplemented('InheritHandles');
  if poUsePipes in FProcessOptions   then WriteNotImplemented('poUsePipes');
  if poStderrToOutput in FProcessOptions then WriteNotImplemented('poStderrToOutput');
  if poNoConsole in FProcessOptions  then WriteNotImplemented('poNoConsole');
  if poNewConsole in FProcessOptions then WriteNotImplemented('poNewConsole');
  if poDefaultErrorMode in FProcessOptions then WriteNotImplemented('poDefaultErrorMode');
  if poNewProcessGroup in FProcessOptions  then WriteNotImplemented('poNewProcessGroup');
  if poDebugProcess in FProcessOptions     then WriteNotImplemented('poDebugProcess');
  if poDebugOnlyThisProcess in FProcessOptions then WriteNotImplemented('poDebugOnlyThisProcess');
{-$endif}

  Path := Trim(FCommandLine);
  I    := Pos(' ', Path);

  if 0 < I then
  begin
    Params := Trim(Copy(Path, I + 1, Length(Path) - I));
    Path   := Copy(Path, 1, I - 1);
  end else
    Params := '';

  if (0 < Length(Path)) and (Path[1] <> '/') and (Pos('/', Path) = 0) then
  begin
    FullPath := IncludeTrailingPathDelimiter(GetCurrentDir) + Path;
    if not FileExists(Path) then
      FullPath := ExeSearch(Path, GetEnvironmentVariable('PATH'));
  end else
    FullPath := Path;

  WriteDebug('Path  : "', FullPath, '"');
  WriteDebug('Params: "', Params, '"');

  System.Assign(F, FullPath);
  System.Reset(F);

  if IOResult <> 0 then
    raise MakeError(IOResult);

  KF := PKosFile(FileRec(F).Handle);
  KF^.Position := (QWord(@Params[1]) << 32) or 1;  { bit0 -- debug mode }
  KF^.Size := 0;
  KF^.Data := nil;

  PID := kos_startapp(KF);

  System.Close(F);

  if PID < 0 then
    raise MakeError(-PID);

  FProcessHandle := PID;
  FThreadHandle  := PID;
  FProcessId     := PID;

  FThreadSlot := kos_getthreadslot(PID);

  WriteDebug('Process ID  : ', PID);
  WriteDebug('Process Slot: ', FThreadSlot);

  if not InjectKosXPC(Pid, Length(DataS) + 1, @DataS[1]) then
    raise MakeError(-2);

  FRunning := True;

  if poWaitOnExit in FProcessOptions then
    WaitOnExit;
end;

function TProcess.WaitOnExit: Boolean;
var
  Info : TKosThreadInfo;
  Delay: Integer;
begin
  if (kos_threadinfo(@Info, FThreadSlot) = 0) or (Info.ThreadID <> ThreadHandle) then
    Result := True else
  begin
    { @todo: (?) KOS_THREAD_TERMINATING_OK, KOS_THREAD_TERMINATING_EXC }
    Delay := 1;
    while Info.Status <> KOS_THREAD_FREE_SLOT do
    begin
      if Info.ThreadID <> Handle then Break;
      kos_delay(Delay);
      kos_threadinfo(@Info, FThreadSlot);
      if Delay < 20 then Inc(Delay, 1);
     {WriteLn(System.StdErr, 'ThreadInfo.Status: ', Info.Status);}
    end;
    Result := True;
  end;

  if Result then
    FRunning := not PeekExitStatus();
end;

function TProcess.Suspend: Longint;
begin
  Result := -1;
end;

function TProcess.Resume: LongInt;
begin
  Result := -1;
end;

function TProcess.Terminate(AExitCode: Integer): Boolean;
begin
  if KillThread(Handle) = 0 then
    Result := WaitOnExit() else
    Result := False;
end;

procedure TProcess.SetShowWindow(Value: TShowWindowOptions);
begin
  FShowWindow := Value;
end;

procedure TProcess.CloseProcessHandles;
begin
end;

function TProcess.PeekExitStatus: Boolean;
var
  Info: TKosThreadInfo;
begin
  if (kos_threadinfo(@Info, FThreadSlot) = 0) or (Info.ThreadID <> ThreadHandle) then
    Result := True else
    Result := Info.Status = KOS_THREAD_FREE_SLOT;

  if Result then
    FExitCode := 0 else { @todo: может можем лучше? }
    FExitCode := STILL_ACTIVE;
end;
